-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2020 at 03:17 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mutasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `prov`
--

CREATE TABLE `prov` (
  `objek` varchar(100) NOT NULL,
  `ket_objek` varchar(100) NOT NULL,
  `jumlah` varchar(11) DEFAULT NULL,
  `harga` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prov`
--

INSERT INTO `prov` (`objek`, `ket_objek`, `jumlah`, `harga`) VALUES
('1.3.1.01', 'Tanah', '24', 2147483647),
('1.3.2.01', 'Alat Besar', '7', 330),
('1.3.2.02', 'Alat Angkutan', '45', 2147483647),
('1.3.2.03', 'Alat Bengkel dan Alat Ukur', '', 0),
('1.3.2.04', 'Alat Pertanian', '', 0),
('1.3.2.05', 'Alat Kantor dan Rumah Tangga', '216', 1607731951),
('1.3.2.06', 'Alat Studio Komunikasi dan Pemancar', '2', 25823000),
('1.3.2.07', 'Alat kedokteran dan Alat kesehatan', '', 0),
('1.3.2.08', 'Alat Laboratorium', '', 0),
('1.3.2.09', 'Alat Persenjataan', '', 0),
('1.3.2.10', 'Alat Komputer', '', 0),
('1.3.2.11', 'Alat Eksplorasi', '', 0),
('1.3.2.12', 'Alat Pengeboran', '', 0),
('1.3.2.13', 'ALAT PRODUKSI, PENGELOLAAN DAN PEMURNIAN', '', 0),
('1.3.2.14', 'ALAT BANTU EKSPLORASI', '', 0),
('1.3.2.15', 'ALAT KESELAMATAN KERJA', '', 0),
('1.3.2.16', 'ALAT PERAGA', '', 0),
('1.3.2.17', 'PERALATAN PROSES/PRODUKSI', '', 0),
('1.3.2.18', 'RAMBU RAMBU', '', 0),
('1.3.2.19', 'PERALATAN OLAH RAGA', '1', 13985000),
('1.3.3.01', 'BANGUNAN GEDUNG', '78', 2147483647),
('1.3.3.02', 'MONUMEN', '', 0),
('1.3.3.03', 'BANGUNAN MENARA', '', 0),
('1.3.3.04', 'TUGU TITIK KONTROL/PASTI', '14', 1921069800),
('1.3.4.01', 'JALAN DAN JEMBATAN', '', 0),
('1.3.4.02', 'BANGUNAN AIR', '', 0),
('1.3.4.03', 'INSTALASI', '', 0),
('1.3.4.04', 'JARINGAN', '1', 179871120),
('1.3.5.01', 'BAHAN PERPUSTAKAAN', '', 0),
('1.3.5.02', 'BARANG BERCORAK KESENIAAN/KEBUDAYAAN/OLAH RAGA', '', 0),
('1.3.5.03', 'HEWAN', '', 0),
('1.3.5.04', 'BIOTA PERAIRAN', '', 0),
('1.3.5.05', 'TANAMAN', '', 0),
('1.3.5.06', 'BARANG KOLEKSI NON BUDAYA', '', 0),
('1.3.5.07', 'ASET TETAP DALAM RENOVASI', '', 0),
('1.3.6.01', 'KONSTRUKSI DALAM PENGERJAAN', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `prov`
--
ALTER TABLE `prov`
  ADD PRIMARY KEY (`objek`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
