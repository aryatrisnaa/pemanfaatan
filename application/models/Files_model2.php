<?php
class Files_model2 extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function insertfile2($file){
		return $this->db->insert('M_Pemanfaatan', $file);
	}
	public function insertPembayaran($file){
		return $this->db->insert('M_Pembayaran', $file);
	}

	public function download($id){
		$query = $this->db->get_where('upload',array('id'=>$id));
		return $query->row_array();
	}

	public function delete_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function getjenis()
	{
		return $this->db->query('SELECT * from JenisManfaat')->result();
	}

	public function getBmd()
	{	
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
			return $this->db->query("SELECT * from M_MasterBMD where statusBMD = 'PENGELOLA'")->result();
		}
		else{
			return $this->db->query("SELECT * from M_MasterBMD where statusBMD = '$PB'")->result();
		}
		
	}

	public function getPeruntukan()
	{
		return $this->db->query('SELECT * from M_Peruntukan')->result();
	}

	public function getSelisih($id)
	{
		return $this->db->query("SELECT a.id, a.alamatBMD, a.luasT, a.luasB, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		a.luasT - (sum (b.luasTK)) as selisihT,  a.luasB - (sum (b.luasBK)) as selisihB
        from AsetInfo.dbo.M_MasterBMD a 
        left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		where a.id = '$id' 
		group by a.id, a.luasT, a.alamatBMD, a.luasB")->row();
	}

	// Aset Pemanfataan
	public function all_master_count2()
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		} else {
		  $query = $this
		  ->db
		  ->where("b.statusBMD", $PB)
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		}
  
	  return $query->num_rows();
	}

	//tablel master
	public function all_master_data2($limit, $start, $dir)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->order_by("a.tgl_input", $dir)
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		} else {
		  $query = $this
		  ->db
		  ->limit($limit, $start)
		  ->where("b.statusBMD", $PB)
		  ->order_by("a.tgl_input", $dir)
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		}
  
	  if ($query->num_rows() > 0) {
		return $query->result();
	  } else {
		return null;
	  }
	}
  
	public function search_master_count2($search)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		  $query = $this
		  ->db
		  ->like("(d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100))", $search)
		  ->or_like("c.KetWil", $search)
		  ->or_like("b.alamatBMD", $search) 
		  ->or_like("a.namaPemohon", $search)
		  ->or_like("e.KetJenis", $search) 
		  ->or_like("b.statusBMD", $search)  
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		} else {
		  $query = $this
		  ->db
		  ->group_start()
		  ->like("(d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100))", $search)
		  ->or_like("c.KetWil", $search)
		  ->or_like("b.alamatBMD", $search) 
		  ->or_like("a.namaPemohon", $search)
		  ->or_like("e.KetJenis", $search)  
		  ->group_end() 
		  ->where("b.statusBMD", $PB)
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		}
  
	  return $query->num_rows();
	}
  
	public function search_master_data2($limit, $start, $dir, $search)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		  $query = $this
		  ->db
		  ->limit($limit, $start)
		  ->like("(d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100))", $search)
		  ->or_like("c.KetWil", $search)
		  ->or_like("b.alamatBMD", $search) 
		  ->or_like("a.namaPemohon", $search)
		  ->or_like("e.KetJenis", $search)  
		  ->or_like("b.statusBMD", $search)  
		  ->order_by("a.tgl_input", $dir)
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		} else {
			$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->group_start()
		  ->like("(d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100))", $search)
		  ->or_like("c.KetWil", $search)
		  ->or_like("b.alamatBMD", $search) 
		  ->or_like("a.namaPemohon", $search)
		  ->or_like("e.KetJenis", $search) 
		  ->group_end() 
		  ->where("b.statusBMD", $PB)
		  ->order_by("a.tgl_input", $dir)
		  ->join("M_MasterBMD b","b.id=a.idBMD","inner")
		  ->join("ListKota c","b.kota=c.KdWil","inner")
		  ->join("M_Peruntukan d","d.idPeruntukan=left(a.idPeruntukan,5)","inner")
		  ->join("JenisManfaat e","e.KdJenis=a.idJenis","inner")
		  ->select("a.id, b.statusBMD, b.jenisObjek, b.alamatBMD, c.KetWil, a.luasTK, a.luasBK, a.namaPemohon, a.alamatPemohon, a.jangkasewa, (d.ketPeruntukan + ' ' + SUBSTRING(a.idPeruntukan,7,100)) as Peruntukan, e.KetJenis")
		  ->get("M_Pemanfaatan a");
		}
  
	  if ($query->num_rows() > 0) {
		return $query->result();
	  } else {
		return null;
	  }
	}

	public function export_report_pemanfaatan()
	{
		$sql = "SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, e.KetWil, a.luasT, a.luasB, a.nilaiWajar, b.luasTK, b.luasBK, b.dasarSK,
		b. noKeputusan, b.dasarPerjanjian,b.noPerjanjian, c.KetJenis, b.namaPemohon, b.alamatPemohon, b.noPemohon, b.jangkasewa, b.tglMulai, b.tglSelesai,
		(d.ketPeruntukan + ' ' + SUBSTRING(b.idPeruntukan,7,100)) as Peruntukan from AsetInfo.dbo.M_MasterBMD a
			left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
 			left join AsetInfo.dbo.JenisManfaat c on b.idJenis=c.KdJenis
 			left join AsetInfo.dbo.M_Peruntukan d on left(b.idPeruntukan,5)=d.idPeruntukan
 			left join AsetInfo.dbo.ListKota e on a.kota=e.KdWil
 			order by b.tglMulai DESC";

		// if($nopol != null){
		// $sql .= " and np.id = '".$nopol."' ";
		// }
		// if($bahan_bakar != null){
		// $sql .= " and bahan_bakar = '".$bahan_bakar."' ";
		// }
		
		// if($awal==$akhir){
		// $sql .= " and date(kbb.tgl) = '".$awal."' ";
		// }
		// else{
		// $sql .= " and date(kbb.tgl) BETWEEN '".$awal."' AND '".$akhir."' ";
		// }
		
		// $sql .= " ORDER BY kbb.tgl, np.no_polisi, name ";
		
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
		return $query->result();
		} else {
		return null;
		}
	}
}
?>