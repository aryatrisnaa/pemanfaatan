<?php
class Files_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
	
	}
	
	// public function getAllFiles(){
	// 	return $this->db->query("SELECT id, tahun, kdMlk, 
	// 	j13101  jKIBA, 
	// 	h13101 hKIBA, 
	// 	j13201 + j13202 + j13203 + j13204 + j13205 + j13206 + j13207 + j13208 + j13209 + j13210 + j13211 + j13212 + j13213 + j13214 + j13215 + j13216 + j13217 + j13218 + j13219 jKIBB,
	// 	h13201 + h13202 + h13203 + h13204 + h13205 + h13206 + h13207 + h13208 + h13209 + h13210 + h13211 + h13212 + h13213 + h13214 + h13215 + h13216 + h13217 + h13218 + h13219 hKIBB,
	// 	j13301 + j13302 + j13303 + j13304 jKIBC, 
	// 	h13301 + h13302 + h13303 + h13304 hKIBC,
	// 	j13401 + j13402 + j13403 + j13404 jKIBD, 
	// 	h13401 + h13402 + h13403 + h13404 hKIBD,
	// 	j13501 + j13502 + j13503 + j13504 + j13505 + j13506 + j13507 jKIBE,
	// 	h13501 + h13502 + h13503 + h13504 + h13505 + h13506 + h13507 hKIBE,
	// 	j13601 jKIBF, h13601 hKIBF
	// 	from AsetInfo.dbo.rekapasetttp")->result();
	// }
	
	public function insertfile($file){
		return $this->db->insert('M_MasterBMD', $file);
	}

	public function insertfile2($file){
		return $this->db->insert('AsetSiapKerjasama', $file);
	}

	public function insertfileLain($file){
		return $this->db->insert('RekapAsetLain', $file);
	}

	public function download($id){
		$query = $this->db->get_where('upload',array('id'=>$id));
		return $query->row_array();
	}

	public function delete_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function getWil()
	{
		return $this->db->query("SELECT * from ListKota where KdWil <> '33.00'")->result();
	}

	public function getjenis()
	{
		return $this->db->query('SELECT * from JenisManfaat')->result();
	}



	//tablel master
	public function all_master_count()
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
			$query = $this
			->db
			
			->join("ListKota c","a.kota=c.KdWil","inner")
			->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
			->get("M_MasterBMD a");
		}
		else{
			$query = $this
			->db
			->where("a.statusBMD", $PB)
			->join("ListKota c","a.kota=c.KdWil","inner")
			->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
			->get("M_MasterBMD a");
		}
		
  
	  return $query->num_rows();
	}

	public function all_master_data($limit, $start, $dir)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
			$query = $this
			->db
			->limit($limit, $start)
			->order_by("tgl_input", $dir)
			
			->join("ListKota c","a.kota=c.KdWil","inner")
			->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
			->get("M_MasterBMD a");
		}
		else{
			$query = $this
			->db
			->limit($limit, $start)
			->order_by("tgl_input", $dir)
			->where("a.statusBMD", $PB)
			->join("ListKota c","a.kota=c.KdWil","inner")
			->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
			->get("M_MasterBMD a");
		}
  
	  if ($query->num_rows() > 0) {
		return $query->result();
	  } else {
		return null;
	  }
	}
  
	public function search_master_count($search)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->or_like("a.luasB", $search)
		  ->or_like("a.luasT", $search)
		  ->or_like("a.alamatBMD", $search)
		  ->or_like("a.statusBMD", $search)
		  ->like("c.KetWil", $search)
		  
		  ->join("ListKota c","a.kota=c.KdWil","inner")
		  ->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
		  ->get("M_MasterBMD a");
		}
		else{
			$query = $this
		  ->db
		  ->group_start()
		  ->or_like("a.luasB", $search)
		  ->or_like("a.luasT", $search)
		  ->or_like("a.alamatBMD", $search)
		  ->or_like("a.statusBMD", $search)
		  ->like("c.KetWil", $search)
		  ->group_end() 
		  ->where("a.statusBMD", $PB)
		  
		  ->join("ListKota c","a.kota=c.KdWil","inner")
		  ->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
		  ->get("M_MasterBMD a");
		}
  
	  return $query->num_rows();
	}
  
	public function search_master_data($limit, $start, $dir, $search)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->or_like("a.luasB", $search)
		  ->or_like("a.luasT", $search)
		  ->or_like("a.alamatBMD", $search)
		  ->or_like("a.statusBMD", $search)
		  ->like("c.KetWil", $search)
		  ->order_by("tgl_input", $dir)
		  
		  ->join("ListKota c","a.kota=c.KdWil","inner")
		  ->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
		  ->get("M_MasterBMD a");
		} else {
			$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->group_start()
		  ->or_like("a.luasB", $search)
		  ->or_like("a.luasT", $search)
		  ->or_like("a.alamatBMD", $search)
		  ->or_like("a.statusBMD", $search)
		  ->like("c.KetWil", $search)
		  ->group_end() 
		  ->where("a.statusBMD", $PB)
		  ->order_by("tgl_input", $dir)
		  
		  ->join("ListKota c","a.kota=c.KdWil","inner")
		  ->select("a.id,a.statusBMD,a.jenisObjek,a.kondisiBMD,a.KIBA,a.KIBC,a.alamatBMD,c.KetWil kota,a.luasT,a.luasB,a.nilaiWajar,a.latitude,a.longtitude,a.photoA,a.photoB,a.tgl_input")
		  ->get("M_MasterBMD a");
		}
  
	  if ($query->num_rows() > 0) {
		return $query->result();
	  } else {
		return null;
	  }
	}

	// Aset Siap Kerjasama
	//tablel idle
	public function all_idle_count()
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  
		//   ->group_by('SuratMasuk.no_surat')
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->where("sisaT > 0")
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		} else{
			$query = $this
		  ->db
		  
		//   ->group_by('SuratMasuk.no_surat')
		  ->group_start()
		  ->or_where("sisaT > 0")
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->group_end()
		  ->where("statusBMD", $PB)
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		}
  
	  return $query->num_rows();
	}

	public function all_idle_data($limit, $start, $col, $dir)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->order_by($col, $dir)
		  
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->where("sisaT > 0")
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		}
		else{
			$query = $this
			->db
			->limit($limit, $start)
			->order_by($col, $dir)
			
			->group_start()
		    ->or_where("sisaT > 0")
		    ->or_where("sisaT IS NULL")
		    ->or_where("sisaB > 0")
		    ->or_where("sisaB IS NULL")
		    ->group_end()
		    ->where("statusBMD", $PB)
			->select("*")
			->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
			a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
			a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
			from AsetInfo.dbo.M_MasterBMD a 
			left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
			left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
			group by a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		  }
  
	  if ($query->num_rows() > 0) {
		return $query->result();
	  } else {
		return null;
	  }
	}
  
	public function search_idle_count($search)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->group_start()
		  ->where("sisaT > 0")
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->group_end() 
		  ->group_start()
		  ->or_like("luasB", $search)
		  ->or_like("luasT", $search)
		  ->or_like("alamatBMD", $search)
		  ->or_like("statusBMD", $search)
		  ->like("KetWil", $search)
		  ->group_end()
		  
		  
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		} else {
			$query = $this
		  ->db
		  ->group_start()
		  ->where("a.statusBMD", $PB)
		  ->or_where("sisaT > 0")
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->group_end() 
		  ->group_start()
		  ->or_like("luasB", $search)
		  ->or_like("luasT", $search)
		  ->or_like("alamatBMD", $search)
		  ->or_like("statusBMD", $search)
		  ->like("KetWil", $search)
		  ->group_end()
		  
		  
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		}
	  return $query->num_rows();
	}
  
	public function search_idle_data($limit, $start, $col, $dir, $search)
	{
		$this->load->library('session');
		$pbsubk = $this->session->userdata('PBSubK');
		$PB = $this->session->userdata('NamaLengkap');
		if($pbsubk == '040301.00000.00000'){
		$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->group_start()
		  ->where("sisaT > 0")
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->group_end() 
		  ->group_start()
		  ->or_like("luasB", $search)
		  ->or_like("luasT", $search)
		  ->or_like("alamatBMD", $search)
		  ->or_like("statusBMD", $search)
		  ->like("KetWil", $search)
		  ->group_end()
		  ->order_by($col, $dir)
		  
		  
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id,a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD,c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		} else {
			$query = $this
		  ->db
		  ->limit($limit, $start)
		  ->group_start()
		  ->where("a.statusBMD", $PB)
		  ->or_where("sisaT > 0")
		  ->or_where("sisaT IS NULL")
		  ->or_where("sisaB > 0")
		  ->or_where("sisaB IS NULL")
		  ->group_end() 
		  ->group_start()
		  ->or_like("luasB", $search)
		  ->or_like("luasT", $search)
		  ->or_like("alamatBMD", $search)
		  ->or_like("statusBMD", $search)
		  ->like("KetWil", $search)
		  ->group_end()
		  ->order_by($col, $dir)
		  
		  
		  ->select("*")
		  ->get("(SELECT a.id, a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, 
		  a.luasT, a.luasB, a.nilaiWajar, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		  a.luasT - (sum (b.luasTK)) as sisaT,  a.luasB - (sum (b.luasBK)) as sisaB
		  from AsetInfo.dbo.M_MasterBMD a 
		  left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		  left join asetinfo.dbo.ListKota c on a.kota=c.kdwil
		  group by a.id,a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD,c.ketwil, a.luasT, a.luasB, a.nilaiWajar) as innerTable");
		}
  
	  if ($query->num_rows() > 0) {
		return $query->result();
	  } else {
		return null;
	  }
	}
	
}
?>