<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_model extends CI_Model
{

  // public function all_kota_count()
  // {
  //     $query = $this
  //       ->db
  //       ->group_by("ListKota.KetWil")
  //       ->group_by("ListKota.KdWil")
  //       ->where("ListKota.KdWil <> '33.00'")
  //       ->join("AsetInfo.dbo.M_Pemanfaatan","M_MasterBMD.id=M_Pemanfaatan.idBMD","left")
  //       ->join("AsetInfo.dbo.M_MasterBMD","ListKota.KdWil=M_MasterBMD.kota","left")
  //       ->select("ListKota.KetWil, ListKota.KdWil,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '001' THEN 1 ELSE 0 END)   AS Sewa,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '002' THEN 1 ELSE 0 END)   AS BGSBSG,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '003' THEN 1 ELSE 0 END)   AS PinjamPakai,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '004' THEN 1 ELSE 0 END)   AS KSPKSPi,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '005' THEN 1 ELSE 0 END)   AS BlmOptimal,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '006' THEN 1 ELSE 0 END)   AS PihakLain,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '007' THEN 1 ELSE 0 END)   AS KSPemerintah")
  //       ->get("AsetInfo.dbo.ListKota");

  //   return $query->num_rows();
  // }

  // public function all_kota_data($limit, $start, $col, $dir)
  // {
  //     $query = $this
  //       ->db
  //       ->limit($limit, $start)
  //       ->order_by($col, $dir)
  //       ->group_by("ListKota.KetWil")
  //       ->group_by("ListKota.KdWil")
  //       ->where("ListKota.KdWil <> '33.00'")
  //       ->join("AsetInfo.dbo.M_Pemanfaatan","M_MasterBMD.id=M_Pemanfaatan.idBMD","left")
  //       ->join("AsetInfo.dbo.M_MasterBMD","ListKota.KdWil=M_MasterBMD.kota","left")
  //       ->select("ListKota.KetWil, ListKota.KdWil,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '001' THEN 1 ELSE 0 END)   AS Sewa,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '002' THEN 1 ELSE 0 END)   AS BGSBSG,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '003' THEN 1 ELSE 0 END)   AS PinjamPakai,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '004' THEN 1 ELSE 0 END)   AS KSPKSPi,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '005' THEN 1 ELSE 0 END)   AS BlmOptimal,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '006' THEN 1 ELSE 0 END)   AS PihakLain,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '007' THEN 1 ELSE 0 END)   AS KSPemerintah")
  //       ->get("AsetInfo.dbo.ListKota");

  //   if ($query->num_rows() > 0) {
  //     return $query->result();
  //   } else {
  //     return null;
  //   }
  // }

  // public function search_kota_count($search)
  // {
  //     $query = $this
  //       ->db
  //       ->like("ListKota.KetWil", $search)
  //       ->group_by("ListKota.KetWil")
  //       ->group_by("ListKota.KdWil")
  //       ->where("ListKota.KdWil <> '33.00'")
  //       ->join("AsetInfo.dbo.M_Pemanfaatan","M_MasterBMD.id=M_Pemanfaatan.idBMD","left")
  //       ->join("AsetInfo.dbo.M_MasterBMD","ListKota.KdWil=M_MasterBMD.kota","left")
  //       ->select("ListKota.KetWil, ListKota.KdWil,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '001' THEN 1 ELSE 0 END)   AS Sewa,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '002' THEN 1 ELSE 0 END)   AS BGSBSG,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '003' THEN 1 ELSE 0 END)   AS PinjamPakai,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '004' THEN 1 ELSE 0 END)   AS KSPKSPi,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '005' THEN 1 ELSE 0 END)   AS BlmOptimal,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '006' THEN 1 ELSE 0 END)   AS PihakLain,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '007' THEN 1 ELSE 0 END)   AS KSPemerintah")
  //       ->get("AsetInfo.dbo.ListKota");

  //   return $query->num_rows();
  // }

  // public function search_kota_data($limit, $start, $col, $dir, $search)
  // {
  //     $query = $this
  //       ->db
  //       ->limit($limit, $start)
  //       ->like("ListKota.KetWil", $search)
  //       ->order_by($col, $dir)
  //       ->group_by("ListKota.KetWil")
  //       ->group_by("ListKota.KdWil")
  //       ->where("ListKota.KdWil <> '33.00'")
  //       ->join("AsetInfo.dbo.M_Pemanfaatan","M_MasterBMD.id=M_Pemanfaatan.idBMD","left")
  //       ->join("AsetInfo.dbo.M_MasterBMD","ListKota.KdWil=M_MasterBMD.kota","left")
  //       ->select("ListKota.KetWil, ListKota.KdWil,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '001' THEN 1 ELSE 0 END)   AS Sewa,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '002' THEN 1 ELSE 0 END)   AS BGSBSG,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '003' THEN 1 ELSE 0 END)   AS PinjamPakai,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '004' THEN 1 ELSE 0 END)   AS KSPKSPi,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '005' THEN 1 ELSE 0 END)   AS BlmOptimal,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '006' THEN 1 ELSE 0 END)   AS PihakLain,
  //                 SUM(CASE WHEN M_Pemanfaatan.idJenis = '007' THEN 1 ELSE 0 END)   AS KSPemerintah")
  //       ->get("AsetInfo.dbo.ListKota");

  //   if ($query->num_rows() > 0) {
  //     return $query->result();
  //   } else {
  //     return null;
  //   }
  // }

  public function show_lokasiData(){

		return $this->db->query("SELECT * from  AsetInfo.dbo.M_MasterBMD a inner join AsetInfo.dbo.ListKota b on a.kota=b.KdWil");
	}

  public function rekap_jml(){

		return $this->db->query("SELECT
    SUM(CASE WHEN a.idJenis = '001' THEN 1 ELSE 0 END)   AS Sewa,
    SUM(CASE WHEN a.idJenis = '002' THEN 1 ELSE 0 END)   AS BGSBSG,
    SUM(CASE WHEN a.idJenis = '003' THEN 1 ELSE 0 END)   AS KSPKSPi,
    SUM(CASE WHEN a.idJenis = '004' THEN 1 ELSE 0 END)   AS PinjamPakai,
    SUM(CASE WHEN a.idJenis = '005' THEN 1 ELSE 0 END)   AS BlmOptimal,
    SUM(CASE WHEN a.idJenis = '006' THEN 1 ELSE 0 END)   AS PihakLain,
    SUM(CASE WHEN a.idJenis = '007' THEN 1 ELSE 0 END)   AS KSPemerintah
    from asetinfo.dbo.M_Pemanfaatan a inner join AsetInfo.dbo.M_MasterBMD b on a.idBMD=b.id");
	}

  // public function get_kota()
  // {
  //   $kota = $this->db->query("SELECT KetWil from AsetInfo.dbo.ListKota where KdWil != '33.00'");
  //   return $result=$kota->row();
  // }

}