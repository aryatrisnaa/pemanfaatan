<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>File Upload Form</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/upload/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<link href="<?php echo base_url('assets/css/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/css/style-responsive.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container container-fluid">
			<div class="navbar-header">
				<ul class="nav navbar-nav">
					<li><a>Upload File Data Aset</a></li>
				</ul>
				<ul class="nav navbar-nav" style="margin-left:850px;">
					<li><a class="btn btn-light" href="<?php echo base_url('Login/logout');?>" class="btn-get-started">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container"><br><br>
			<div class="col-sm-12 row justify-content-center">
				<table id="example" class="display" style="width:100%">
					<thead>
						<tr>
							<th>Filename</th>
							<th>Deskripsi</th>
							<th>Wilayah</th>
							<th>Tanggal Upload</th>
							<th>Download</th>
							<th>Hapus</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($files as $file){
							?>
							<tr>
								<td><?php echo $file->nama; ?></td>
								<td><?php echo $file->deskripsi; ?></td>
								<td><?php echo $file->KetWil; ?></td>
								<td><?php echo $file->tanggal; ?></td>
								<td align="center"><a href="<?php echo base_url().'File/download/'.$file->id; ?>" class="btn btn-success btn-sm col-sm-10"><span class="glyphicon glyphicon-download-alt"></a></td>
								<td align="center"><a onclick="delete_data('<?php echo base_url('File/delete_data/'.$file->id) ?>')" 
													href="#"class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></a></td>	
							</tr>
								<?php
							}
							?>
					</tbody>
				</table>
			</div>
		</div>
		
		<?php $this->load->view("partials/modal.php") ?>
		
		<script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable( {
					"order": [[ 3, "desc" ]]
				} );
			} );
		</script>
		<script type="text/javascript">
			function delete_data(url){
				$('#btn-delete').attr('href', url);
				$('#deleteModal').modal();
			}
		</script>
	</body>
	</html>