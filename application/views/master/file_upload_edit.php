<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

  <style type="text/css">
    .atas {
      margin-top: 80px;
    }
    .tengah {
      margin: auto;
    }
    .navbar-nav > .active > a {
      color: white;    
    }
    .nav-item > a:hover {
      color: white;
    }
    .nav-item > a {
      color: white;
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-11">

    <a style="font-size:20px;cursor:pointer;color:#fff" href="<?php echo base_url('File');?>"><i class="icofont-bubble-left"></i> Kembali</a>  
    <h1 class="logo mr-auto"><a href="#header" class="scrollto"></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('Login/logout');?>" class="btn-get-started">Logout</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="upload" class="upload">
      <div class="atas" data-aos="fade-up">
      <?php
      if($this->session->flashdata('success')){
        ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php
      }
    
      if($this->session->flashdata('error')){
        ?>
        <div class="alert alert-danger text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php
      }?>
     
      <div class="tab-content">
        <div id="tetap" class="tab-pane active"><br>

          <div class="sub-title">
            <h2>Edit Data BMD</h2>
          </div>
          
          <div class="container col-lg-8 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="total">
              <div class="col-lg-12"><br>
              <form method="POST" action="<?php echo base_url(); ?>File/update_data" enctype="multipart/form-data">
               <br>
                <div style="font-size: 14px">
                  <input type="hidden" class="form-control hidden" name="edit_id" value='<?php echo($edit->id);?>'>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">STATUS BMD </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_statusBMD" id="edit_statusBMD" class="form-control filter">
                          <option value="<?php echo($edit->statusBMD);?>"><?php echo($edit->statusBMD);?></option>
                          <option value="PENGGUNA">PENGGUNA</option>
                          <option value="PENGELOLA">PENGELOLA</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">JENIS OBJEK </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_jenisObjek" id="edit_jenisObjek" class="form-control filter">
                          <option value="<?php echo($edit->jenisObjek);?>"><?php echo($edit->jenisObjek);?></option>
                          <option value="TANAH">TANAH</option>
                          <option value="BANGUNAN">BANGUNAN</option>
                          <option value="TANAH & BANGUNAN">TANAH & BANGUNAN</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">KONDISI BMD </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_kondisiBMD" id="edit_kondisiBMD" class="form-control filter">
                          <option value="<?php echo($edit->kondisiBMD);?>"><?php echo($edit->kondisiBMD);?></option>
                          <option value="BAIK">BAIK</option>
                          <option value="RUSAK RINGAN">RUSAK RINGAN</option>
                          <option value="RUSAK BERAT">RUSAK BERAT</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">KIBA </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_KIBA" name="edit_KIBA" min="0" class="form-control" value='<?php echo($edit->KIBA);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">KIBC </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_KIBC" name="edit_KIBC" min="0" class="form-control" value='<?php echo($edit->KIBC);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">ALAMAT </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_alamatBMD" name="edit_alamatBMD" min="0" class="form-control" value='<?php echo($edit->alamatBMD);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">KOTA </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_kota" id="edit_kota" class="form-control filter">
                        <option value="<?php echo($edit->kota);?>"><?php echo($edit->KetWil);?></option>
                        <?php foreach($kota as $row):?>
                            <option value="<?php echo $row->KdWil;?>"><?php echo $row->KetWil;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS TANAH</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="edit_luasT" name="edit_luasT" min="0" class="form-control" value='<?php echo($edit->luasT);?>' ></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS BANGUNAN</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="edit_luasB" name="edit_luasB" min="0" class="form-control" value='<?php echo($edit->luasB);?>' ></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">NILAI WAJAR </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_nilaiWajar" name="edit_nilaiWajar" min="0" class="form-control" value='<?php echo($edit->nilaiWajar);?>'></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LATITUDE </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_latitude" name="edit_latitude" min="0" class="form-control" value='<?php echo($edit->latitude);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LONGTITUDE </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_longtitude" name="edit_longtitude" min="0" class="form-control" value='<?php echo($edit->longtitude);?>' required></div>
                  </div>
                  <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FOTO ASET (max. 4 file) </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="userfile[]" multiple="multiple" required>
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : jpeg | jpg | png</p></div>
                  </div> -->
                </div>
                <center><button type="submit" class="btn btn-primary col-lg-12" style="margin-top:10px; margin-bottom:10px;">Update Data</button></center>
              </form>
              </div>
            </div>
          </div>

        </div>
        

      </div>
      </div>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php $this->load->view("partials/footer.php") ?>
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>

  <script>
    // Restricts input for the given textbox to the given inputFilter.
    function setInputFilter(textbox, inputFilter) {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      });
    }

    setInputFilter(document.getElementById("noP"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("hargaTotal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("hargaTahunan"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });

      // currency format
    function checkKey() {
        var clean = this.value.replace(/[^0-9,]/g, "")
                              .replace(/(,.*?),(.*,)?/, "$1");
        // don't move cursor to end if no change
        if (clean !== this.value) this.value = clean;
    }
    document.querySelector('edit_nilaiWajar').oninput = checkKey;
  </script>

<script>
$(document).ready(function(){
  $("#datepicker").datepicker({
     format: "yyyy",
     viewMode: "years", 
     minViewMode: "years",
     autoclose:true
  });   
})
$(document).ready(function(){
  $("#datepicker2").datepicker({
     format: "yyyy",
     viewMode: "years", 
     minViewMode: "years",
     autoclose:true
  });   
})

</script>

<script type='text/javascript'>
  $(document).ready(function() {
    $('#edit_kota').select2({
      placeholder: 'Pilih Kabupaten',
      allowClear: true
    });       
  });

  $(document).ready(function() {
    $('#status').select2({
      placeholder: 'Pilih Jenis Manfaat',
      allowClear: true
    });       
  });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

</body>

</html>