<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

  <style type="text/css">
    .atas {
      margin-top: 80px;
    }
    .tengah {
      margin: auto;
    }
    .navbar-nav > .active > a {
      color: white;    
    }
    .nav-item > a:hover {
      color: white;
    }
    .nav-item > a {
      color: white;
    }
    #upload:before {
      position: fixed;
    }
    
    /* sidebar */
    body {
      font-family: "Lato", sans-serif;
      transition: background-color .5s;
    }
    .sidenav {
      height: 100%;
      width: 0;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #111;
      overflow-x: hidden;
      transition: 0.5s;
      padding-top: 60px;
    }

    .sidenav a {
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 20px;
      color: #818181;
      display: block;
      transition: 0.3s;
    }

    .sidenav a:hover {
      color: #f1f1f1;
    }

    .sidenav .closebtn {
      position: absolute;
      top: 0;
      right: 25px;
      font-size: 36px;
      margin-left: 50px;
    }

    /* slideshow */
    .carousel-control-prev-icon,
    .carousel-control-next-icon {
    display: inline-block;
    width: 70px;
    height: 70px;
    background-size: 100% 100%;
    }

    #main {
      transition: margin-left .5s;
    }

    @media screen and (max-height: 450px) {
      .sidenav {padding-top: 15px;}
      .sidenav a {font-size: 18px;}
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-11">

      <a style="font-size:20px;cursor:pointer;color:#fff" href="<?php echo base_url('File');?>"><i class="icofont-bubble-left"></i> Kembali</a>
      <h1 class="logo mr-auto"><a href="#header" class="scrollto"></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('Login/logout');?>" class="btn-get-started">Logout</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
  <div id="main">
      
    <!-- ======= About Us Section ======= -->
    <section id="upload" class="upload">
      <div class="atas" data-aos="fade-up">
        <?php
        if($this->session->flashdata('success')){
            ?>
            <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php
        }
        
        if($this->session->flashdata('error')){
            ?>
            <div class="alert alert-danger text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php
        }?>
     
        <div class="tab-content">
            <div class="sub-title">
                <h2>Detail Data BMD</h2>
            </div>
            <table style="width: 100%;" class="col-lg-12">
                <tr>
                    <td style="width: 45%;vertical-align:top">
                    <div class="container col-lg-11 " style="margin-left:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12"><br>
                                <div class="col-lg-12">
                                <table class="table">
                                    <tr>
                                        <td><b>Status BMD</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->statusBMD);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Obyek</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->jenisObjek);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kondisi BMD</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->kondisiBMD);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat BMD</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->alamatBMD);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kab/Kota</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->KetWil);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Luas Tanah</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->luasT);?> m2</td>
                                    </tr>
                                    <tr>
                                        <td><b>Luas Bangunan</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail->luasB);?> m2</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nilai Wajar</b></td>
                                        <td><b>:</b></td>
                                        <td>Rp. <?php echo number_format($detail->nilaiWajar,2,",",".");?></td>
                                    </tr>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="container col-lg-11" style="margin-left:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12 container">
                                <a href="https://www.google.com/maps/place/<?php echo($detail->latitude);?> <?php echo($detail->longtitude);?>" target="blank" class="btn btn-primary col-lg-12" ><i class="fa fa-map" style="margin-right:8px"></i> Google Maps</a>
                            </div>
                        </div>
                    </div>
                    </td>
                    <td style="width: 55%;vertical-align:top">
                    <div class="container col-lg-11 " style="margin-right:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12"><br>
                                <div class="col-lg-12">
                                    <div id="carouselExample" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <?php if($detail->photoA == !null) {?>
                                            <div class="carousel-item active">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail->photoA); ?>" width="100%"></embed> 
                                            </div>
                                            <?php }?>
                                            <?php if($detail->photoB == !null) {?>
                                            <div class="carousel-item">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail->photoB); ?>" width="100%"></embed>
                                            </div>
                                            <?php }?>
                                            <?php if($detail->photoC == !null) {?>
                                            <div class="carousel-item">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail->photoC); ?>" width="100%"></embed>
                                            </div>
                                            <?php }?>
                                            <?php if($detail->photoD == !null) {?>
                                            <div class="carousel-item">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail->photoD); ?>" width="100%"></embed>
                                            </div>
                                            <?php }?>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>                            
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>             
                                </div>
                            </div>
                        </div>
                    </div>
                    </td>
                </tr>
            </table>
            
        </div>
      </div>
    </section><!-- End About Us Section -->

    </div><!-- End #main -->

    <!-- ======= Footer ======= -->
    <!-- </?php $this->load->view("partials/footer.php") ?> -->
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
    <div id="preloader"></div>

    <?php $this->load->view("partials/js.php") ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

</body>

</html>