<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.js'></script>
   <link href='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css' rel='stylesheet' />

   <style>
          #map { 
               height: 80vh; 
          }
          .div-map { 
               margin-top: 0px;
          }
          .marker {
              background-image: url('../main/img/zona2.png');
              background-size: cover;
              width: 50px;
              height: 50px;
              margin-top:-19.5px;
              border-radius: 50%;
              cursor: pointer;
          }

          .mapboxgl-popup-content {
               width: 425px;
               text-align: left;
               font-family: 'Open Sans', sans-serif;
               padding: 15px;
               line-height: 1;
          }
/*           
          .info {
              margin-top:30px;
              height: 55px;
              background-color:rgb(252, 203, 69, 0.87);
              position: fixed;
              z-index:99;
              color: #000;
          } */
          #table-scroll {
            height:470px;
            overflow:auto;  
            width:100%;
            background:#fff;
            color:black;
            font-size: 15px;
          }
          .font{
            font-size: 15px;
          }

          .vertical-center {
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
          }
  }
   </style>

</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-10">

      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="#header" class="logo scrollto"><img src="main/img/jateng.png" alt="prov. jateng" class="img-fluid"></a>
      <!-- <h1 class="logo" style="margin-left:10px"><a class="scrollto">PROFIL ASET PEMANFAATAN</a></h1> -->

      <nav class="nav-menu d-none d-lg-block ml-auto">
        <ul>
          <li class="active"><a href="#header">Home</a></li>
          <li><a href="#about">Data</a></li>
          <li><a href="#cta">Peta</a></li>
          <a href="<?php echo base_url('Login');?>" class="btn-get-started">Login</a>
          <!-- <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Deep Drop Down</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li> -->
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade col-lg-10" data-ride="carousel">

      <!-- Slide 1 -->
      <div class="carousel-item active">
        <div class="col-lg-12" style="float:left;">
          <div class="col-lg-5" style="float:left; text-align:center; ">
            <img class="animate__animated animate__fadeInLeft" style="width:72%" src="main/img/kerjasama.png" alt="kerjasama">
          </div>
          <div class="col-lg-7" style="float:left; text-align:center; height:37vh;">
            <div class="vertical-center">
              <div class="col-lg-12">
                <h2 class="animate__animated animate__fadeInRight">DATA PEMANFAATAN ASET PROVINSI JAWA TENGAH</span></h2>
              </div>
              <div class="col-lg-12 text-center">
                <a href="#about" class="hero-btn align-middle animate__animated animate__fadeInUp scrollto" >Mulai</a>
              </div>  
            </div>         
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
          <div class="container col-lg-10" data-aos="fade-up">
            <div class="col-lg-6 rekap" style="text-align:center;float:left">
              <div class="col-lg-6" style="float:left; margin-bottom:30px;">
                <div class="total col-lg-12">
                  <b><a class="judul">SEWA</a></b><br>
                  <b><a class="count" style="color: #E9A111;"><?php echo ($rekap->Sewa);?></a></b>
                </div>
              </div>
              <div class="col-lg-6" style="float:left; margin-bottom:30px;">
                <div class="total col-lg-12">
                  <b><a class="judul">BGS/BSG</a></b><br>
                  <b><a class="count" style="color: #E9A111;"><?php echo ($rekap->BGSBSG);?></a></b>
                </div>
              </div>
              <div class="col-lg-6" style="float:left; margin-bottom:30px;">
                <div class="total col-lg-12">
                  <b><a class="judul">KSP/KSPI</a></b><br>
                  <b><a class="count" style="color: #E9A111;"><?php echo ($rekap->KSPKSPi);?></a></b>
                </div>
              </div>
              <div class="col-lg-6" style="float:left; margin-bottom:30px;">
                <div class="total col-lg-12">
                  <b><a class="judul">Pinjam Pakai</a></b><br>
                  <b><a class="count" style="color: #E9A111;"><?php echo ($rekap->PinjamPakai);?></a></b>
                </div>
              </div>
              <div class="col-lg-6" style="float:left; margin-bottom:30px;">
                <div class="total col-lg-12">
                  <b><a class="judul">Ijin Operasional <br> Pihak Lain</a></b><br>
                  <b><a class="count" style="color: #E9A111;"><?php echo ($rekap->PihakLain);?></a></b>
                </div>
              </div>
              <div class="col-lg-6" style="float:left; margin-bottom:30px;">
                <div class="total col-lg-12">
                  <b><a class="judul">Kerja sama <br> antar Pemerintah</a></b><br>
                  <b><a class="count" style="color: #E9A111;"><?php echo ($rekap->KSPemerintah);?></a></b>
                </div>
              </div>
              <div class="col-lg-12" style="float:left; margin-bottom:30px;">
                <div class="total2 col-lg-12">
                  <b><a class="judul" style="color:#fff">Aset Siap Kerjasama</a></b>
                </div>
              </div>
            </div>
            <div class="col-lg-6 rekap" style="float:left; margin-bottom:0px;">
                <div class="total col-lg-12" style="text-align:center">
                  <b><a class="judul">Galeri Aset Pemanfaatan</a></b><br>
                </div>
              </div>
            <div class="col-lg-6 " style="float:left">
              <div id="carouselExample" class="carousel slide total" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <embed type="image/jpg" src="<?php echo base_url('main/img/landmark0.jpg'); ?>" width="100%"></embed> 
                    </div>
                    <div class="carousel-item">
                        <embed type="image/jpg" src="<?php echo base_url('main/img/landmark2.jpg'); ?>" width="100%"></embed>
                    </div>
                    <div class="carousel-item">
                        <embed type="image/jpg" src="<?php echo base_url('main/img/landmark3.jpg'); ?>" width="100%"></embed>
                    </div>
                    <div class="carousel-item">
                        <embed type="image/jpg" src="<?php echo base_url('main/img/landmark4.jpg'); ?>" width="100%"></embed>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>                            
                </a>
                <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
              </div> 
            </div>
          </div>
        </section>      
    <!-- End About Us Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="row" data-aos="zoom-in">
          <div class="col-lg-9 text-center text-lg-left">
            <h3>Peta Aset Pemanfaatan</h3>
            <p> Sebaran Lokasi Aset Pemanfaatan Provinsi Jawa Tengah</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a href="#services" class="cta-btn align-middle animate__animated animate__fadeInUp scrollto" >Lihat Peta</a>
          </div>
        </div>
        
      </div>
    </section><!-- End Cta Section -->
    
    <!-- ======= Kab Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container col-lg-10 col-md-10 col-sm-10">
        <div id="map"></div>
      </div>
    
      <div id="modal01" class="modal" onclick="this.style.display='none'">
        <span class="close">&times;</span>
        <img class="modal-content" style="width:auto; height:auto" id="img01">
        <div id="caption"></div>
      </div>
    </section><!-- End Kab Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php $this->load->view("partials/map.php")?>
  <?php $this->load->view("partials/footer.php") ?>
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>
  <!-- </?php $this->load->view("partials/chart.php") ?> -->

  <script type="text/javascript">
      var table = $('#kota-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "Home/data_wilayah",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "KetWil", 'sortable': false},
          { "data": "Sewa", 'sortable': false},
          { "data": "BGSBSG", 'sortable': false},
          { "data": "PinjamPakai", 'sortable': false},
          { "data": "KSPKSPi", 'sortable': false},
          { "data": "BlmOptimal", 'sortable': false},
          { "data": "action", "className": "text-center", 'sortable': false}
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#kota-table_filter input').unbind();
      var dtable = $('#kota-table').dataTable().api();
      $('#kota-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#kota-table').DataTable().ajax.reload();
      });

      function data_sub(id){
        var data_id =  id ;
        // var url = '</?php echo site_url('Home/data_sub/') ?>'+data_id;

        window.location = '<?php echo site_url('Home/data_sub/') ?>'+data_id;
        // console.log(url)
        // return false;
      }
  </script>


  <script type="text/javascript">
     

      function data_sub(id){
        var data_id =  id ;
        // var url = '</?php echo site_url('Home/data_sub/') ?>'+data_id;

        window.location = '<?php echo site_url('Home/data_sub/') ?>'+data_id;
        // console.log(url)
        // return false;
      }
  </script>

</body>

</html>