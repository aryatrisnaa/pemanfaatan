<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <style type="text/css">
    #table-scroll {
        height:430px;
        overflow:auto;  
        width:100%;
        background:#fff;
        color:black;
        font-size: 14px;
      }
      .font{
        font-size: 14px;
      }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="<?php echo base_url('');?>" class="scrollto">SIM ASET JATENG</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('');?>"><i class="icofont-bubble-left"></i> Kembali</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex justify-content-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <!-- Slide 1 -->
      <div class="carousel-item active">
        <div class="carousel-container"><br>
          <form method="POST" action="<?php echo base_url(); ?>Home/data_sub/<?php echo($sub->KdWil);?>" enctype="multipart/form-data">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <h2 class="animate__animated animate__fadeInDown">DATA ASET <br><?php echo($sub->KetWil);?></h2>
            </div><br>
            <div class="col-lg-12 col-md-12 col-sm-12" style="float:left">
              <p class="col-lg-4 col-md-4 col-sm-4 animate__animated animate__fadeInUp" style="float:left; margin-top:8px">TAHUN AUDITED:</p>
              <select class="col-lg-4 col-md-4 col-sm-4 form-control" style="float:left" id="tahunAud" name="tahunAud">
                <option ><?php echo $dropdown; ?></option>
                <?php foreach($tahunaudi as $th) : ?>
                <option value="<?php echo $th->tahun;?>"> <?php echo $th->tahun; ?></option>
                <?php endforeach; ?>
              </select>
              <div class="col-lg-4 col-md-4 col-sm-4" style="float:left; margin-top:-10px">
                <button class="btn-get-started " type="submit"><a style="color:black">Lihat Data</a></button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <ul class="nav nav-tabs nav-pills nav-justified">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#home">Aset Tetap</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu1">Aset Lainnya</a>
      </li>
    </ul>
    
    <div class="tab-content">
      <div id="home" class="tab-pane active">
        <!-- ======= About Us Section ======= -->
        <section id="sub" class="sub">
          <div style="margin-top:50px"data-aos="fade-up">

            <div class="sub-title">
              <h2>Data Aset Tetap <br></h2>
            </div>
            
            <div class="container col-md-5 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
              <div class="total" style="text-align:center">
                <h4 style="margin-top:20px"><a>Nilai : Rp.<?php echo number_format($totalSub->Harga ,2,",",".");?></a></h4>
                <h4><a>Jumlah      : <?php echo number_format($totalSub->Jumlah ,0,",",".");?></a></h4>
              </div>
            </div>
            <div class="row" style="margin-left:35px; margin-right:35px">
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-island-alt"></i>
                  <h4><a>Tanah</a></h4>
                  <p><b>Total Nilai : Rp.<?php echo number_format($sumSub->hKIBA ,2,",",".");?></b></p>
                  <p><b>Jumlah : <?php echo number_format($sumSub->jKIBA ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="200">
                <div class="icon-box">
                  <i class="icofont-car"></i>
                  <h4><a>Peralatan dan Mesin</a></h4>
                  <p><b>Total Nilai : Rp.<?php echo number_format($sumSub->hKIBB ,2,",",".");?></b></p>
                  <p><b>Jumlah : <?php echo number_format($sumSub->jKIBB ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="300">
                <div class="icon-box">
                  <i class="icofont-building-alt"></i>
                  <h4><a>Gedung dan Bangunan</a></h4>
                  <p><b>Total Nilai : Rp.<?php echo number_format($sumSub->hKIBC ,2,",",".");?></b></p>
                  <p><b>Jumlah : <?php echo number_format($sumSub->jKIBC ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="400">
                <div class="icon-box">
                  <i class="icofont-road"></i>
                  <h4><a>Jalan, Jaringan dan Irigasi</a></h4>
                  <p><b>Total Nilai : Rp.<?php echo number_format($sumSub->hKIBD ,2,",",".");?></b></p>
                  <p><b>Jumlah : <?php echo number_format($sumSub->jKIBD ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="500">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Aset Tetap Lainnya</a></h4>
                  <p><b>Total Nilai : Rp.<?php echo number_format($sumSub->hKIBE ,2,",",".");?></b></p>
                  <p><b>Jumlah : <?php echo number_format($sumSub->jKIBE ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="600">
                <div class="icon-box">
                  <i class="icofont-under-construction-alt"></i>
                  <h4><a>Konstruksi dalam Pengerjaan</a></h4>
                  <p><b>Total Nilai : Rp.<?php echo number_format($sumSub->hKIBF ,2,",",".");?></b></p>
                  <p><b>Jumlah : <?php echo number_format($sumSub->jKIBF ,0,",",".");?></b></p>
                </div>
              </div>
            
            </div>

          </div>
        </section><!-- End About Us Section -->
        
        <!-- ======= Team Section ======= -->
        <section id="team" class="team section-bg">
          <div class="container" data-aos="fade-up">
            
            <div class="section-title" style="margin-top:-15px">
              <h2>Detail Objek</h2>
              <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
            </div>
            <div class="row justify-content-center">
              <div class="col-lg-11 col-md-11 col-xs-11" style="top:-30px; bottom:-50px;">
                <div id="table-scroll">
                  <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th width="50%" style="text-align:center">Keterangan Objek</th>
                        <th width="5%" style="text-align:center">Jumlah</th>
                        <th width="35%" style="text-align:center">Nilai</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>TANAH</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13101 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13101 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT BESAR</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13201 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13201 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT ANGKUTAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13202 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13202 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT BENGKEL DAN ALAT UKUR</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13203 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13203 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT PERTANIAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13204 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13204 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT KANTOR DAN RUMAH TANGGA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13205 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13205 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT STUDIO KOMUNIKASI DAN PEMANCAR</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13206 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13206 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT KEDOKTERAN DAN ALAT KESEHATAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13207 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13207 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT LABORATORIUM</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13208 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13208 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT PERSENJATAAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13209 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13209 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT KOMPUTER</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13210 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13210 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT EKSPLORASI</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13211 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13211 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT PENGEBORAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13212 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13212 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT PRODUKSI, PENGELOLAAN DAN PEMURNIAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13213 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13213 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT BANTU EKSPLORASI</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13214 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13214 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT KESELAMATAN KERJA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13215 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13215 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ALAT PERAGA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13216 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13216 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>PERALATAN PROSES/PRODUKSI</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13217 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13217 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>RAMBU RAMBU</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13218 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13218 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>PERALATAN OLAH RAGA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13219 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13219 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BANGUNAN GEDUNG</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13301 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13301 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>MONUMEN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13302 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13302 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BANGUNAN MENARA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13303 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13303 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>TUGU TITIK KONTROL/PASTI</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13304 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13304 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>JALAN DAN JEMBATAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13401 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13401 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BANGUNAN AIR</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13402 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13402 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>INSTALASI</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13403 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13403 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>JARINGAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13404 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13404 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BAHAN PERPUSTAKAAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13501 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13501 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BARANG BERCORAK KESENIAAN/KEBUDAYAAN/OLAH RAGA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13502 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13502 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>HEWAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13503 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13503 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BIOTA PERAIRAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13504 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13504 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>TANAMAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13505 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13505 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>BARANG KOLEKSI NON BUDAYA</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13506 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13506 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>ASET TETAP DALAM RENOVASI</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13507 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13507 ,2,",",".");?></a>
                          </td>
                        </tr>
                        <tr>
                          <td width="50%" style="text-align:justify">
                            <a>KONSTRUKSI DALAM PENGERJAAN</a>
                          </td>
                          <td width="5%" style="text-align:right">
                            <a><?php echo number_format($detSub->j13601 ,0,",",".");?></a>
                          </td>
                          <td width="35%" style="text-align:right">
                            <a>Rp.<?php echo number_format($detSub->h13601 ,2,",",".");?></a>
                          </td>
                        </tr>
                    </tbody>
                  </table>
                </div><br><br>
              <div class="arrow">
                <span></span>
                <span></span>
                <span></span>
              </div>
              </div>
            </div>
          </div>
          <span class="d-flex justify-content-center" style="font-size: 13px; color:#065fad;">*scroll tabel untuk melihat data objek lainnya</span>
        </section><!-- End Team Section -->
      </div>

      <div id="menu1" class=" tab-pane fade"><br>
        <section id="about" class="about">
          <div data-aos="fade-up">
            <div class="section-title">
              <h2>Data Aset Lainnya</h2>
            </div>
            
            <!-- <div class="container col-md-5" data-aos="fade-up" data-aos-delay="100">
              <div class="total" style="text-align:center">
                <h4 style="margin-top:20px"><a>Harga : Rp.</?php echo number_format($total->Harga ,0,",",".");?></a></h4>
                <h4><a>Jumlah      : </?php echo number_format($total->Jumlah ,0,",",".");?></a></h4>
              </div>
            </div> -->
            <div class="row" style="margin-left:35px; margin-right:35px">
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Pemanfaatan BMD pada Kemitraan dengan Pihak Ketiga</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiPmnftnKemitraanKetiga ,2,",",".");?></b></p>
                  <p><b>Jumlah      : <?php echo number_format($sublain->JmlPmnftnKemitraanKetiga ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Aset Tetap yang Tidak Digunakan dalam Operasional Pemerintah</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiAsetTtpNonPmrnth ,2,",",".");?></b></p>
                  <p><b>Jumlah      : <?php echo number_format($sublain->JmlAsetTtpNonPmrnth ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Aset Tak Berwujud</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiAtb ,2,",",".");?></b></p>
                  <p><b>Jumlah      : <?php echo number_format($sublain->JmlAtb ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Aset Lain-lain</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiAsetLain ,2,",",".");?></b></p>
                  <p><b>Jumlah      : <?php echo number_format($sublain->JmlAsetLain ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Aset Rusak Berat/Usang</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiAsetRusak ,2,",",".");?></b></p>
                  <p><b>Jumlah      : <?php echo number_format($sublain->JmlAsetRusak ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Aset Lain-lainnya</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiAsetLain_nya ,2,",",".");?></b></p>
                  <p><b>Jumlah      : <?php echo number_format($sublain->JmlAsetLain_nya ,0,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Nilai Amortisasi Aset Tidak Berwujud</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->NilaiAmortisasiAtb ,2,",",".");?></b></p>
                </div>
              </div>
              <div class="col-md-6 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box">
                  <i class="icofont-sub-listing"></i>
                  <h4><a>Nilai Penyusutan Aset Lainnya</a></h4>
                  <p><b>Nilai : Rp.<?php echo number_format($sublain->PenyusutanAsetLainnya ,2,",",".");?></b></p>
                </div>
              </div>
              <div style="margin-top:27px" class="container col-md-4" data-aos="fade-up" data-aos-delay="100">
                <div class="total">
                  <h4 style="margin-top:25px; text-align:center"><a>Jumlah Bentuk Pemanfaatan BMD <br>pada Kemitraan dengan Pihak Ketiga:</a></h4>
                  <table class="table row justify-content-center">
                    <tr>
                      <th>SEWA</th>
                      <th>: <?php echo number_format($sublain->JmlPmnftnSewa ,0,",",".");?></th>
                    </tr>
                    <tr>
                      <th>KSP</th>
                      <th>: <?php echo number_format($sublain->JmlPmnftnKsp ,0,",",".");?></th>
                    </tr>
                    <tr>
                      <th>BGS/BSG</th>
                      <th>: <?php echo number_format($sublain->JmlPmnftnBgs_Bsg ,0,",",".");?></th>
                    </tr>
                    <tr>
                      <th>KSPI</th>
                      <th>: <?php echo number_format($sublain->JmlPmnftnKspi ,0,",",".");?></th>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section><!-- End Aset lainnya Section -->
      </div>
    </div>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php $this->load->view("partials/footer.php") ?>
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>

<!-- datatable -->
<script>
    $('#dataTable').DataTable({
        "ordering": false

    });

</script>

</body>

</html>