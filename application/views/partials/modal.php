 
<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apakah kamu yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <!-- <span aria-hidden="true">×</span> -->
        </button>
      </div>
      <div class="modal-body">Data akan dihapus dan tidak bisa dikembalikan.</div>
      <div class="modal-footer">
        <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal">Kembali</button>
        <a id="btn-delete" class="btn btn-danger btn-sm" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>
