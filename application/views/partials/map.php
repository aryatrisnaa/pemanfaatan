<script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiamFsYW5rZWx1YXIiLCJhIjoiY2s4NDQ0emduMHNudjNybnozejJlcHdiNyJ9.dmJLmc37eArBnnGEMPOGjQ';
        var map = new mapboxgl.Map({
            center: [110.087134, -7.307074],
            zoom: 7.75,
            container: 'map',
            style: 'mapbox://styles/jalankeluar/ck93r5j5m34fb1ip9x1za7fy4'
        });
        
        map.addControl(new mapboxgl.NavigationControl());

        var geojson = {
            type: 'FeatureCollection',
            features: [
            <?php foreach ($lokasi as $tik):?>
                {
                    type: 'Feature',
                    geometry: {
                                type: 'Point',
                                coordinates: [<?php echo $tik->longtitude?>, <?php echo $tik->latitude?>]
                                },
                    properties: {
                                title: '<table class="col-md-12" style="border:none;"><tr><td><img style="margin-left:5px" src="<?php echo base_url('main/img/jateng.png')?>" width="30px"></td><td><center>Sebaran Aset Pemanfaatan<br>PEMERINTAH PROVINSI <br>JAWA TENGAH </td> <td width="50px"> </td> </tr> </table>',
                                description: ''
        +'<br>'
        +'<ul class="nav nav-tabs nav-pills nav-justified" role="tablist">'
            +'<li class="nav-item col-lg-4 ">'
                +'<a class="nav-link active" data-toggle="pill" href="#informasi">Informasi</a>'
            +'</li>'
            +'<li class="nav-item col-lg-4">'
                +'<a class="nav-link" data-toggle="pill" href="#galeri">Galeri</a>'
            +'</li>'
            +'</li>'
            +'<li class="nav-item col-lg-4">'
                +'<a class="nav-link" data-toggle="pill" href="#gmaps">Google Maps</a>'
            +'</li>'
        +'</ul>'

        +'<div class="tab-content">'
            +'<div id="informasi" class="container tab-pane active"><br>'
                +'<table style="width:100%; margin-left:-15px; text-align:left;">'
                    +'<tr>'
                        +'<td>Jenis Objek</td>'
                        +'<td><span style="margin-left:10px;margin-right:5px">:</span></td>'
                        +'<td><?php echo($tik->jenisObjek);?></td>'
                    +'</tr><br>'
                    +'<tr>'
                        +'<td><br>Alamat</td>'
                        +'<td><br><span style="margin-left:10px;margin-right:5px">:</span></td>'
                        +'<td><br><?php echo($tik->alamatBMD);?></td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td><br>Kota/Kabupaten</td>'
                        +'<td><br><span style="margin-left:10px;margin-right:5px">:</span></td>'
                        +'<td><br><?php echo($tik->KetWil);?></td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td><br>Luas Tanah</td>'
                        +'<td><br><span style="margin-left:10px;margin-right:5px">:</span></td>'
                        +'<td><br><?php echo($tik->luasT);?> m2</td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td><br>Luas Bangunan</td>'
                        +'<td><br><span style="margin-left:10px;margin-right:5px">:</span></td>'
                        +'<td><br><?php echo($tik->luasB);?> m2</td>'
                    +'</tr>'
                +'</table>'
            +'</div>'           
            +'<div id="galeri" class="container tab-pane fade"><br>'
                +'<table style="width:100%">'
                +'<button type="button" class="btn btn-sm btn-primary col-sm-12" id="kib"  onclick="tik(\'<?php echo  $tik->id?>\')" value ="">Tampilkan gambar</button>'
                +'</table>'
            +'</div>'
            +'<div id="gmaps" class="container tab-pane fade"><br>'
                +'<table style="width:100%">'
                +'<a href="https://www.google.com/maps/place/<?php echo($tik->latitude);?> <?php echo($tik->longtitude);?>" target="blank" class="btn btn-primary col-lg-12" ><i class="fa fa-map" style="margin-right:8px"></i> Google Maps</a>'
                +'</table>'
            +'</div>'                
            +'</div>'
        +'</div>'
                                }
                },
            <?php endforeach; ?>
            ]
        };

        // add markers to map
        geojson.features.forEach(function(marker) {

            // create a HTML element for each feature
            var el = document.createElement('div');
            el.className = 'marker';

            // make a marker for each feature and add to the map
            new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(new mapboxgl.Popup({ offset: 10 }) // add popups
            .setHTML('<br><h6><b>' + marker.properties.title + '</b></h6><p style="font-size: 14px">' + marker.properties.description + '</p>'))
            // .setMaxWidth("300px")
            .addTo(map);
        });
  </script>
