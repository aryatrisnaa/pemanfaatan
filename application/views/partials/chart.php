<script>
    var options = {
            series: [{
            data: [<?php foreach ($chart as $p): ?>	
            <?php echo $p->A ?>,
            <?php echo $p->B ?>,
            <?php echo $p->C ?>,
            <?php echo $p->D ?>,
            <?php echo $p->E ?>,
            <?php echo $p->F ?>,
            <?php endforeach; ?>]
            }],
            chart: {
            type: 'bar',
            height: 420
            },
            plotOptions: {
            bar: {
                barHeight: '75%',
                distributed: true,
                horizontal: true,
                dataLabels: {
                position: 'bottom'
                },
            }
            },
            colors: ['#026137', '#01BFCB', '#01923A', '#34495E', '#62b32d', '#01923A'
            ],
            dataLabels: {
            enabled: true,
            textAnchor: 'start',
            style: {
                fontSize: '14px',
                colors: ['#ffff00']
            },
            formatter: function (val, opt) {
                return opt.w.globals.labels[opt.dataPointIndex] 
            },
            offsetX: 0,
            dropShadow: {
                enabled: true
            }
            },
            stroke: {
            width: 1,
            colors: ['#fff']
            },
            xaxis: {
            categories: ['Tanah', 'Peralatan dan Mesin', 'Gedung dan Bangunan', 'Jalan, Jaringan dan irigasi', 'Aset Tetap Lainnya', 'Konstruksi Dalam Pengerjaan'
            ],
            },
            yaxis: {
            labels: {
                show: false
            }
            },
            title: {
                text: 'Prov. Jawa Tengah',
                align: 'center',
                floating: true
            },
            // subtitle: {
            //     text: 'Category Names as DataLabels inside bars',
            //     align: 'center',
            // },
            tooltip: {
            theme: 'dark',
            x: {
                show: false
            },
            y: {
                title: {
                formatter: function () {
                    return ''
                }
                }
            }
            }
            };

            var chart = new ApexCharts(document.querySelector("#chart"), options);
            chart.render();
</script>