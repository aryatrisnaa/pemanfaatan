<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

  <style type="text/css">
    .atas {
      margin-top: 80px;
    }
    .tengah {
      margin: auto;
    }
    .navbar-nav > .active > a {
      color: white;    
    }
    .nav-item > a:hover {
      color: white;
    }
    .nav-item > a {
      color: white;
    }
    #upload:before {
      position: fixed;
    }
    
    /* sidebar */
    body {
      font-family: "Lato", sans-serif;
      transition: background-color .5s;
    }
    .sidenav {
      height: 100%;
      width: 0;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #111;
      overflow-x: hidden;
      transition: 0.5s;
      padding-top: 60px;
    }

    .sidenav a {
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 20px;
      color: #818181;
      display: block;
      transition: 0.3s;
    }

    .sidenav a:hover {
      color: #f1f1f1;
    }

    .sidenav .closebtn {
      position: absolute;
      top: 0;
      right: 25px;
      font-size: 36px;
      margin-left: 50px;
    }

    /* slideshow */
    .carousel-control-prev-icon,
    .carousel-control-next-icon {
    display: inline-block;
    width: 70px;
    height: 70px;
    background-size: 100% 100%;
    }

    #main {
      transition: margin-left .5s;
    }

    @media screen and (max-height: 450px) {
      .sidenav {padding-top: 15px;}
      .sidenav a {font-size: 18px;}
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-11">

      <a style="font-size:20px;cursor:pointer;color:#fff" href="<?php echo base_url('File2');?>"><i class="icofont-bubble-left"></i> Kembali</a>
      <h1 class="logo mr-auto"><a href="#header" class="scrollto"></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('Login/logout');?>" class="btn-get-started">Logout</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
  <div id="main">
      
    <!-- ======= About Us Section ======= -->
    <section id="upload" class="upload">
      <div class="atas" data-aos="fade-up">
        <?php
        if($this->session->flashdata('success')){
            ?>
            <div class="alert alert-success text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php
        }
        
        if($this->session->flashdata('error')){
            ?>
            <div class="alert alert-danger text-center" style="margin-top:20px;">
            <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php
        }?>
     
        <div class="tab-content">
            <div class="sub-title">
                <h2>Detail Data Pemanfaatan</h2>
            </div>
            <table style="width: 100%;" class="col-lg-12">
                <tr>
                    <td style="width: 55%;vertical-align:top">
                    <div class="container col-lg-11 " style="margin-left:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12"><br>
                                <div class="col-lg-12">

                                <li><b><a style="font-size:22px;">Info Aset BMD:</a></b></li><br>
                                <table class="table">
                                    <tr>
                                        <td style="width:40%"><b>Status BMD</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td><?php echo($detail2->statusBMD);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Obyek</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->jenisObjek);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kondisi BMD</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->kondisiBMD);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat BMD</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->alamatBMD);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kab/Kota</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->KetWil);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Luas Tanah</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->luasT);?> m2</td>
                                    </tr>
                                    <tr>
                                        <td><b>Luas Bangunan</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->luasB);?> m2</td>
                                    </tr>
                                </table>
                                <div class="col-lg-12 container">
                                    <a href="https://www.google.com/maps/place/<?php echo($detail2->latitude);?> <?php echo($detail2->longtitude);?>" target="blank" class="btn btn-primary col-lg-12" ><i class="icofont-map" style="margin-right:8px"></i> Google Maps</a>
                                </div>
                                <br><br>
                                    <li><b><a style="font-size:22px;">Info Penyewa:</a></b></li><br>
                                <table class="table">
                                    <tr>
                                        <td><b>Nama Pemohon</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->namaPemohon);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat Pemohon</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->alamatPemohon);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No HP Pemohon</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->noPemohon);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Metode Pemanfaatan</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->KetJenis);?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:40%"><b>Luas Tanah yang Dimanfaatkan</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td><?php echo($detail2->luasTK);?> m2</td>
                                    </tr>
                                    <tr>
                                        <td><b>Luas Bangunan yang Dimanfaatkan</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->luasBK);?> m2</td>
                                    </tr>
                                    <tr>
                                        <td><b>Dasar SK</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->dasarSK);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. Keputusan</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->noKeputusan);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dasar Perjanjian</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->dasarPerjanjian);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. Perjanjian</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->noPerjanjian);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jangka Sewa</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->jangkasewa);?> Tahun</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal Mulai</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->tglMulai);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal Selesai</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->tglSelesai);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Peruntukan</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->Peruntukan);?></td>
                                    </tr>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="container col-lg-11 " style="margin-left:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12"><br>
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <b><a class="mr-auto" style="font-size:22px;">Info Pembayaran</a></b>
                                        <a class="btn btn-primary" style="color:#fff" data-toggle="modal" data-target="#modal_masuk_add_new"><i class="icofont-money" style="margin-right:8px"></i> Form Pembayaran</a>
                                    </div>
                                <br>
                            <?php if($detail2->jangkasewa > 0) {?>
                                <br><li><a style="font-size:20px;">Tahap 1:</a></li>
                                <table class="table">
                                    <tr>
                                        <td style="width:40%"><b>Nominal</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td>Rp. <?php echo number_format($detail2->th1_nominal,2,",",".");?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jatuh Tempo</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th1_tglTempo);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th1_noSTS);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th1_tglSTS);?></td>
                                    </tr>
                                </table>
                            <?php }?>
                            <?php if($detail2->jangkasewa > 1) {?>
                                <br><li><a style="font-size:20px;">Tahap 2:</a></li>
                                <table class="table">
                                    <tr>
                                        <td style="width:40%"><b>Nominal</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td>Rp. <?php echo number_format($detail2->th2_nominal,2,",",".");?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jatuh Tempo</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th2_tglTempo);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th2_noSTS);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th2_tglSTS);?></td>
                                    </tr>
                                </table>
                            <?php }?>
                            <?php if($detail2->jangkasewa > 2) {?>
                                <br><li><a style="font-size:20px;">Tahap 3:</a></li>
                                <table class="table">
                                    <tr>
                                        <td style="width:40%"><b>Nominal</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td>Rp. <?php echo number_format($detail2->th3_nominal,2,",",".");?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jatuh Tempo</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th3_tglTempo);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th3_noSTS);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th3_tglSTS);?></td>
                                    </tr>
                                </table>
                            <?php }?>
                            <?php if($detail2->jangkasewa > 3) {?>
                                <br><li><a style="font-size:20px;">Tahap 4:</a></li>
                                <table class="table">
                                    <tr>
                                        <td style="width:40%"><b>Nominal</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td>Rp. <?php echo number_format($detail2->th4_nominal,2,",",".");?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jatuh Tempo</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th4_tglTempo);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th4_noSTS);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th4_tglSTS);?></td>
                                    </tr>
                                </table>
                            <?php }?>
                            <?php if($detail2->jangkasewa > 4) {?>
                                <br><li><a style="font-size:20px;">Tahap 5:</a></li>
                                <table class="table">
                                    <tr>
                                        <td style="width:40%"><b>Nominal</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td>Rp. <?php echo number_format($detail2->th5_nominal,2,",",".");?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jatuh Tempo</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th5_tglTempo);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No. STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th5_noSTS);?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal STS</b></td>
                                        <td><b>:</b></td>
                                        <td><?php echo($detail2->th5_tglSTS);?></td>
                                    </tr>
                                </table>
                            <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    </td>

                    <td style="width: 45%;vertical-align:top">
                    <div class="container col-lg-11 " style="margin-right:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12">
                                <li><b><a style="font-size:22px;">Foto BMD</a></b></li><br>
                                <div class="col-lg-12">
                                    <div id="carouselExample" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <?php if($detail2->photoA == !null) {?>
                                            <div class="carousel-item active">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail2->photoA); ?>" width="100%"></embed> 
                                            </div>
                                            <?php }?>
                                            <?php if($detail2->photoB == !null) {?>
                                            <div class="carousel-item">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail2->photoB); ?>" width="100%"></embed>
                                            </div>
                                            <?php }?>
                                            <?php if($detail2->photoC == !null) {?>
                                            <div class="carousel-item">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail2->photoC); ?>" width="100%"></embed>
                                            </div>
                                            <?php }?>
                                            <?php if($detail2->photoD == !null) {?>
                                            <div class="carousel-item">
                                                <embed type="image/jpg" src="<?php echo base_url('upload/master/images/'.$detail2->photoD); ?>" width="100%"></embed>
                                            </div>
                                            <?php }?>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>                            
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>             
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php if($detail2->fileSK == !null) {?>
                    <div class="container col-lg-11 " style="margin-right:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12">
                            <li><b><a style="font-size:22px;">File SK</a></b></li><br>
                                <div class="col-lg-12">
                                <embed type="application/pdf" src="<?php echo base_url('upload/manfaat/'.$detail2->fileSK); ?>" width="100%" height="700px"></embed>             
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php }?>
                    <?php if($detail2->filePerjanjian == !null) {?>
                    <div class="container col-lg-11 " style="margin-right:80px" data-aos="fade-up" data-aos-delay="100">
                        <div class="total">
                            <div class="col-lg-12">
                            <li><b><a style="font-size:22px;">File Surat Perjanjian</a></b></li><br>
                                <div class="col-lg-12">
                                <embed type="application/pdf" src="<?php echo base_url('upload/manfaat/'.$detail2->filePerjanjian); ?>" width="100%" height="700px"></embed>             
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    </td>
                </tr>
            </table>
            
        </div>
      </div>
    </section><!-- End About Us Section -->

    </div><!-- End #main -->

    <!-- ======= Footer ======= -->
    <!-- </?php $this->load->view("partials/footer.php") ?> -->
    <!-- End Footer -->

    <!-- ============ MODAL ADD Pembayaran =============== -->
  <div class="modal fade" id="modal_masuk_add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Pembayaran</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <!-- <span aria-hidden="true">×</span> -->
            </button>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>File2/insert_pembayaran" enctype="multipart/form-data">
            <div class="modal-body">
                <div style="font-size: 14px">
                    <input type="hidden" class="form-control hidden" name="idPMF" value='<?php echo($detail2->id);?>'>
                    <?php if($detail2->jangkasewa > 0) {?>
                    <center><b><label class="col-lg-12 col-md-12 col-sm-12">Tahap 1</label></b></center>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Nominal</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th1_nominal" name="th1_nominal" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal Jatuh Tempo</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th1_tglTempo" id="datepicker" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">No. STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th1_noSTS" name="th1_noSTS" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th1_tglSTS" id="datepicker2" class="form-control"></div>
                    </div>
                    <?php }?>

                    <?php if($detail2->jangkasewa > 1) {?>
                    <center><b><label class="col-lg-12 col-md-12 col-sm-12">Tahap 2</label></b></center>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Nominal</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th2_nominal" name="th2_nominal" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal Jatuh Tempo</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th2_tglTempo" id="datepicker3" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">No. STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th2_noSTS" name="th2_noSTS" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th2_tglSTS" id="datepicker4" class="form-control"></div>
                    </div>
                    <?php }?>

                    <?php if($detail2->jangkasewa > 2) {?>
                    <center><b><label class="col-lg-12 col-md-12 col-sm-12">Tahap 3</label></b></center>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Nominal</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th3_nominal" name="th3_nominal" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal Jatuh Tempo</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th3_tglTempo" id="datepicker5" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">No. STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th3_noSTS" name="th3_noSTS" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th3_tglSTS" id="datepicker6" class="form-control"></div>
                    </div>
                    <?php }?>

                    <?php if($detail2->jangkasewa > 3) {?>
                    <center><b><label class="col-lg-12 col-md-12 col-sm-12">Tahap 4</label></b></center>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Nominal</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th4_nominal" name="th4_nominal" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal Jatuh Tempo</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th4_tglTempo" id="datepicker7" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">No. STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th4_noSTS" name="th4_noSTS" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th4_tglSTS" id="datepicker8" class="form-control"></div>
                    </div>
                    <?php }?>

                    <?php if($detail2->jangkasewa > 4) {?>
                    <center><b><label class="col-lg-12 col-md-12 col-sm-12">Tahap 5</label></b></center>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Nominal</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th5_nominal" name="th5_nominal" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal Jatuh Tempo</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th5_tglTempo" id="datepicker9" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">No. STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" id="th5_noSTS" name="th5_noSTS" min="0" class="form-control"></div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                      <label class="col-lg-6 col-md-6 col-sm-6" style="float:left">Tanggal STS</label>
                      <div class="col-lg-6 col-md-6 col-sm-6" style="float:left"><input type="text" name="th5_tglSTS" id="datepicker10" class="form-control"></div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL ADD Pembayaran-->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
    <div id="preloader"></div>

    <?php $this->load->view("partials/js.php") ?>

    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

    <script>
    $(document).ready(function(){
    $("#datepicker").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker2").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker3").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker4").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker5").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker6").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker7").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker8").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker9").datepicker({
        format: "yyyy-mm-dd"
    });   
    })
    $(document).ready(function(){
    $("#datepicker10").datepicker({
        format: "yyyy-mm-dd"
    });   
    })

    //texbox only number format
    function setInputFilter(textbox, inputFilter) {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      });
    }

    setInputFilter(document.getElementById("th1_nominal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("th2_nominal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("th3_nominal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("th4_nominal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("th5_nominal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    </script>

</body>

</html>