<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

  <style type="text/css">
    .atas {
      margin-top: 80px;
    }
    .tengah {
      margin: auto;
    }
    .navbar-nav > .active > a {
      color: white;    
    }
    .nav-item > a:hover {
      color: white;
    }
    .nav-item > a {
      color: white;
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-11">

      <a style="font-size:20px;cursor:pointer;color:#fff" href="<?php echo base_url('File2');?>"><i class="icofont-bubble-left"></i> Kembali</a>  
      <h1 class="logo mr-auto"><a href="#header" class="scrollto"></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('Login/logout');?>" class="btn-get-started">Logout</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="upload" class="upload">
      <div class="atas" data-aos="fade-up">
      <?php
      if($this->session->flashdata('success')){
        ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php
      }
    
      if($this->session->flashdata('error')){
        ?>
        <div class="alert alert-danger text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php
      }?>
     
      <div class="tab-content">
        <div id="tetap" class="tab-pane active"><br>

          <div class="sub-title">
            <h2>Edit Data Pemanfaatan</h2>
          </div>
          
          <div class="container col-lg-8 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="total">
              <div class="col-lg-12"><br>
              <form method="POST" action="<?php echo base_url(); ?>File2/update_data" enctype="multipart/form-data">
               <br>
               <div style="font-size: 14px">
               <input type="hidden" class="form-control hidden" name="edit_id2" value='<?php echo($edit->id);?>'>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">ASET BMD </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_idBMD" id="edit_idBMD" class="form-control filter">
                        <option value="<?php echo($edit->idBMD);?>">- <?php echo($edit->alamatBMD);?> -</option>
                        <?php foreach($bmd as $row):?>
                            <option value="<?php echo $row->id;?>"><?php echo $row->alamatBMD;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS TANAH YANG DI KERJASAMAKAN</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="edit_luasTK" name="edit_luasTK" min="0" class="form-control" value='<?php echo($edit->luasTK);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS BANGUNAN YANG DI KERJASAMAKAN</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="edit_luasBK" name="edit_luasBK" min="0" class="form-control" value='<?php echo($edit->luasBK);?>'required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">DASAR SK KEPUTUSAN </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_dasarSK" id="edit_dasarSK" class="form-control filter">
                          <option value="<?php echo($edit->dasarSK);?>">- <?php echo($edit->dasarSK);?> -</option>
                          <option value="SK GUBERNUR">SK GUBERNUR</option>
                          <option value="SK SEKDA">SK SEKDA</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">No Keputusan</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_noKeputusan" name="edit_noKeputusan" min="0" class="form-control" value='<?php echo($edit->noKeputusan);?>'required></div>
                  </div>
                  <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FILE SK</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="fileSK" required>
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : .pdf</p></div>
                  </div> -->
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">DASAR PERJANJIAN </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_dasarPerjanjian" id="edit_dasarPerjanjian" class="form-control filter">
                          <option value="<?php echo($edit->dasarPerjanjian);?>">- <?php echo($edit->dasarPerjanjian);?> -</option>
                          <option value="SEKDA">SEKDA</option>
                          <option value="KEPALA SKPD">KEPALA SKPD</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">No PERJANJIAN</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_noPerjanjian" name="edit_noPerjanjian" min="0" class="form-control" value='<?php echo($edit->noPerjanjian);?>' required></div>
                  </div>
                  <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FILE PERJANJIAN</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="filePerjanjian" required>
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : .pdf</p></div>
                  </div> -->
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">METODE PEMANFAATAN </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_idJenis" id="edit_idJenis" class="form-control filter">
                      <option value="<?php echo($edit->KdJenis);?>">- <?php echo($edit->KetJenis);?> -</option>
                        <?php foreach($jenisManfaat as $row):?>
                            <option value="<?php echo $row->KdJenis;?>"><?php echo $row->KetJenis;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">NAMA PEMOHON </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" name="edit_namaPemohon" min="0" class="form-control" value='<?php echo($edit->namaPemohon);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">ALAMAT PEMOHON </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_alamatPemohon" name="edit_alamatPemohon" min="0" class="form-control" value='<?php echo($edit->alamatPemohon);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">NO. HP PEMOHON </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="edit_noPemohon" name="edit_noPemohon" min="0" class="form-control" value='<?php echo($edit->noPemohon);?>' required></div>
                  </div>
                  <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FILE IDENTITAS (KTP)</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="fileKTP" required>
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : .pdf</p></div>
                  </div> -->
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">WAKTU SEWA (tahun)</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="edit_jangkasewa" name="edit_jangkasewa" min="0" class="form-control" value='<?php echo($edit->jangkasewa);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">TANGGAL MULAI</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="text" name="edit_tglMulai" id="datepicker" class="form-control" value='<?php echo($edit->tglMulai);?>' required> </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">TANGGAL BERAKHIR</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="text" name="edit_tglSelesai" id="datepicker2" class="form-control" value='<?php echo($edit->tglSelesai);?>' required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">PERUNTUKAN</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="edit_Peruntukan" id="edit_Peruntukan" class="form-control filter">
                      <option value="<?php echo($edit->idPeruntukan);?>">- <?php echo($edit->Peruntukan);?> -</option>
                        <?php foreach($Peruntukan as $row):?>
                            <option value="<?php echo $row->idPeruntukan;?>"><?php echo $row->ketPeruntukan;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                </div>
                <center><button type="submit" class="btn btn-primary col-lg-12" style="margin-top:10px; margin-bottom:10px;">Update Data</button></center>
              </form>
              </div>
            </div>
          </div>

        </div>
        

      </div>
      </div>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php $this->load->view("partials/footer.php") ?>
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>

  <script>
    // Restricts input for the given textbox to the given inputFilter.
    function setInputFilter(textbox, inputFilter) {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      });
    }

    setInputFilter(document.getElementById("noP"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("hargaTotal"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });
    setInputFilter(document.getElementById("hargaTahunan"), function(value) {
    return /^-?\d*[.]?\d*$/.test(value); });

      // currency format
    function checkKey() {
        var clean = this.value.replace(/[^0-9,]/g, "")
                              .replace(/(,.*?),(.*,)?/, "$1");
        // don't move cursor to end if no change
        if (clean !== this.value) this.value = clean;
    }
    document.querySelector('edit_nilaiWajar').oninput = checkKey;
  </script>

<script>
$(document).ready(function(){
  $("#datepicker").datepicker({
     format: "yyyy",
     viewMode: "years", 
     minViewMode: "years",
     autoclose:true
  });   
})
$(document).ready(function(){
  $("#datepicker2").datepicker({
     format: "yyyy",
     viewMode: "years", 
     minViewMode: "years",
     autoclose:true
  });   
})

</script>

<script type='text/javascript'>
  $(document).ready(function() {
    $('#edit_idBMD').select2({
      placeholder: 'Pilih Data BMD',
      allowClear: true
    });       
  });

  $(document).ready(function() {
    $('#edit_dasarSK').select2({
      placeholder: 'Pilih Dasar SK',
      allowClear: true
    });       
  });
  $(document).ready(function() {
    $('#edit_dasarPerjanjian').select2({
      placeholder: 'Pilih Dasar Perjanjian',
      allowClear: true
    });       
  });

  $(document).ready(function() {
    $('#edit_idJenis').select2({
      placeholder: 'Pilih Jenis Manfaat',
      allowClear: true
    });       
  });
  $(document).ready(function() {
    $('#edit_idPeruntukan').select2({
      placeholder: 'Pilih Kabupaten',
      allowClear: true
    });       
  });

</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

</body>

</html>