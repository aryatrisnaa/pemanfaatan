<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">

  <style type="text/css">
    .atas {
      margin-top: 80px;
    }
    .tengah {
      margin: auto;
    }
    .navbar-nav > .active > a {
      color: white;    
    }
    .nav-item > a:hover {
      color: white;
    }
    .nav-item > a {
      color: white;
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-11">

      <a style="font-size:20px;cursor:pointer;color:#fff" href="<?php echo base_url('File2');?>"><i class="icofont-bubble-left"></i> Kembali</a>  
      <h1 class="logo mr-auto"><a href="#header" class="scrollto"></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('Login/logout');?>" class="btn-get-started">Logout</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="upload" class="upload">
      <div class="atas" data-aos="fade-up">
      <?php
      if($this->session->flashdata('success')){
        ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php
      }
    
      if($this->session->flashdata('error')){
        ?>
        <div class="alert alert-danger text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php
      }?>
     
      <div class="tab-content">
        <div id="tetap" class="tab-pane active"><br>

          <div class="sub-title">
            <h2>Input Data Pemanfaatan</h2>
          </div>
          
          <div class="container col-lg-8 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="total">
              <div class="col-lg-12"><br>
              <form method="POST" action="<?php echo base_url(); ?>File2/insert" enctype="multipart/form-data">
               <br>

                <div style="font-size: 14px">
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">ASET BMD </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select type="text" name="idBMD" id="idBMD" class="form-control form-control-line setinput" value="">
                        <option value="">- Pilih ASET BMD -</option>
                        <?php foreach($bmd as $row):?>
                            <option value="<?php echo $row->id;?>"><?php echo $row->alamatBMD;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS TANAH BMD</label>
                    <div class="form-group col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input class="form-control col-lg-4 col-md-4 col-sm-4" style="float:left" type="number" name="luasT" min="0" readonly>
                      <a class="col-lg-2 col-md-2 col-sm-2" style="float:left; text-align:right"> sisa : </a>
                      <input class="form-control col-lg-4 col-md-4 col-sm-4" style="float:left"type="number" name="selisihT" min="0" readonly>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS BANGUNAN BMD</label>
                    <div class="form-group col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input class="form-control col-lg-4 col-md-4 col-sm-4" style="float:left" type="number" name="luasB" min="0" readonly>
                      <a class="col-lg-2 col-md-2 col-sm-2" style="float:left; text-align:right"> sisa : </a>
                      <input class="form-control col-lg-4 col-md-4 col-sm-4" style="float:left"type="number" name="selisihB" min="0" readonly>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS TANAH YANG DI KERJASAMAKAN</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="luasTK" name="luasTK" min="0" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">LUAS BANGUNAN YANG DI KERJASAMAKAN</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="number" id="luasBK" name="luasBK" min="0" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">DASAR SK KEPUTUSAN </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="dasarSK" id="dasarSK" class="form-control filter">
                          <option value="">- Pilih Dasar SK -</option>
                          <option value="SK GUBERNUR">SK GUBERNUR</option>
                          <option value="SK SEKDA">SK SEKDA</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">No Keputusan</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="noKeputusan" name="noKeputusan" min="0" class="form-control"></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FILE SK</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="fileSK">
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : .pdf</p></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">DASAR PERJANJIAN </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="dasarPerjanjian" id="dasarPerjanjian" class="form-control filter">
                          <option value="">- Pilih Dasar Perjanjian -</option>
                          <option value="SEKDA">SEKDA</option>
                          <option value="KEPALA SKPD">KEPALA SKPD</option>
                          <option value="KEPALA BALAI/UPT">KEPALA BALAI/UPT</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">No PERJANJIAN</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="noPerjanjian" name="noPerjanjian" min="0" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FILE PERJANJIAN</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="filePerjanjian" required>
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : .pdf</p></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">METODE PEMANFAATAN </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="idJenis" id="idJenis" class="form-control filter">
                        <option value="">- Pilih Metode Pemanfaatan -</option>
                        <?php foreach($jenisManfaat as $row):?>
                            <option value="<?php echo $row->KdJenis;?>"><?php echo $row->KetJenis;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">NAMA PEMOHON </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="namaPemohon" name="namaPemohon" min="0" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">ALAMAT PEMOHON </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="alamatPemohon" name="alamatPemohon" min="0" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">NO. HP PEMOHON </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="noPemohon" name="noPemohon" min="0" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">FILE IDENTITAS (KTP)</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <input type="file" name="fileKTP">
                      <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : .pdf</p></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">WAKTU SEWA (tahun)</label>
                    <div class="col-lg-4 col-md-4 col-sm-8" style="float:left">
                      <select name="jangkasewa" id="jangkasewa" class="form-control filter" required>
                          <option value="">- Jangka Waktu Sewa -</option>
                          <option value="1">1 Tahun</option>
                          <option value="2">2 Tahun</option>
                          <option value="3">3 Tahun</option>
                          <option value="4">4 Tahun</option>
                          <option value="5">5 Tahun</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">TANGGAL MULAI</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="text" name="tglMulai" id="datepicker" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">TANGGAL BERAKHIR</label>
                    <div class="col-lg-2 col-md-4 col-sm-8" style="float:left"><input type="text" name="tglSelesai" id="datepicker2" class="form-control" required></div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left">PERUNTUKAN</label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left">
                      <select name="Peruntukan" id="Peruntukan" class="form-control filter">
                        <option value="">-Pilih Peruntukan-</option>
                        <?php foreach($Peruntukan as $row):?>
                            <option value="<?php echo $row->idPeruntukan;?>"><?php echo $row->ketPeruntukan;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12" style="float:left; text-align:left;">
                    <label class="col-lg-4 col-md-4 col-sm-4" style="float:left"> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8" style="float:left"><input type="text" id="keterangan" name="keterangan" min="0" class="form-control" disabled="true" value="" required></div>
                    <!-- <input id="textbox" disabled="true" type="text"   value=""  name=""/> -->
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label>File:</label><br>
                  <input type="file" name="upload">
                  <p style="font-size:12px; color:#A6ACAF ">*file yang didukung : doc | docx| xls | xlsx | pdf | zip | rar</p>
                </div> -->
                <center><button type="submit" class="btn btn-primary col-lg-12" style="margin-top:10px; margin-bottom:10px;">Insert Data</button></center>
              </form>
              </div>
            </div>
          </div>

        </div>
        

      </div>
      </div>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php $this->load->view("partials/footer.php") ?>
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>

  <script>
    // Restricts input for the given textbox to the given inputFilter.
  $(document).ready(function() {
    $('#idBMD').select2({
      placeholder: 'Pilih Data BMD',
      allowClear: true
    });       
    $('body').on('change','.setinput',function(){
    $.ajax({
        url: 'getluasT',
        type:'POST',
        data:{idBMD:$(this).val()},
        success:function(response){
          var result = JSON.parse(response);
            $('input[name="luasT"]').val(result.luasT);
            $('input[name="luasB"]').val(result.luasB);
            $('input[name="selisihT"]').val(result.selisihT);
            $('input[name="selisihB"]').val(result.selisihB);
        }
    })
  });
  });
  </script>

<script>
$(document).ready(function(){
  $("#datepicker").datepicker({
     format: "yyyy-mm-dd"
  });   
})
$(document).ready(function(){
  $("#datepicker2").datepicker({
     format: "yyyy-mm-dd"
  });   
})

document.getElementById('Peruntukan').addEventListener('change', function() {
    if (this.value == 'PM010') {
        document.getElementById('keterangan').disabled = false;   
    } else {
        document.getElementById('keterangan').disabled = true;
    }
});
</script>
<!-- 
<script type='text/javascript'>
  $(document).ready(function() {
    $('#idBMD').select2({
      placeholder: 'Pilih Data BMD',
      allowClear: true
    });       
  });
</script> -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

</body>

</html>