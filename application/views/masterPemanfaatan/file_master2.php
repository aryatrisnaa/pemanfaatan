<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
  <style>
    #header a {
      color: #fff;
    }
  </style>
</head>

<body>
  <?php $this->load->view("partials/navbar.php") ?>
  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="upload" class="upload">
      <div class="atas" data-aos="fade-up">
      <?php
      if($this->session->flashdata('success')){
        ?>
        <div class="alert alert-success text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('success'); ?>
        </div>
        <?php
      }
    
      if($this->session->flashdata('error')){
        ?>
        <div class="alert alert-danger text-center" style="margin-top:20px;">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php
      }?>
     
      <div class="tab-content">
        <div id="tetap" class="tab-pane active"><br>

          <div class="sub-title">
            <h2>Master Data Aset Pemanfaatan</h2>
          </div>
          <div class="container col-lg-10 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="total">
              <div class="col-lg-12"><br>
               <div class="col-lg-12">
                 <a href="<?php echo base_url('File2/user');?>" class='btn btn-sm btn-warning' style='color:#000; float:right;'><i class='icofont-plus'> </i>Insert Data</a>
                 <a class="btn btn-sm btn-info" style='color:#fff' id="laporan"><i class='icofont-print'> </i> Export Excel</a>
               </div><br><br>
                <div data-aos="fade-up">
                  <div class="table-responsive container col-lg-12 font" style="font-size:14px">
                    <table class="table table-bordered table-striped" id="master-table" width="100%" cellspacing="0">
                      <thead>
                        <tr class="info">
                          <th style="text-align:center">No</th>
                          <th style="text-align:center">STATUS BMD</th>
                          <th style="text-align:center">JENIS PEMANFAATAN</th>
                          <th style="text-align:center">JENIS OBJEK</th>
                          <th style="text-align:center">ALAMAT</th>
                          <th style="text-align:center">KOTA</th>
                          <th style="text-align:center">LUAS TANAH PEMANFAATAN</th>
                          <th style="text-align:center">LUAS BANGUNAN PEMANFAATAN</th>
                          <th style="text-align:center">NAMA PEMOHON</th>
                          <th style="text-align:center">JANGKA WAKTU</th>
                          <th style="text-align:center">PERUNTUKAN</th>
                          <!-- <th style="text-align:center">TOTAL NILAI PEMANFAATAN</th> -->
                          <th style="text-align:center; width:250px;">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        

      </div>
      </div>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>

  <script type="text/javascript">
      var table = $('#master-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "File2/data_master2",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "statusBMD", 'sortable': false},
          { "data": "KetJenis", 'sortable': false},
          { "data": "jenisObjek", 'sortable': false},
          { "data": "alamatBMD", 'sortable': false},
          { "data": "KetWil", 'sortable': false},
          { "data": "luasTK", 'sortable': false},
          { "data": "luasBK", 'sortable': false},
          { "data": "namaPemohon", 'sortable': false},
          { "data": "jangkasewa", 'sortable': false},
          { "data": "ketPeruntukan", 'sortable': false},
          // { "data": "totalNilai", 'sortable': false},
          { "data": "action", "className": "text-center", 'sortable': false}
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#master-table_filter input').unbind();
      var dtable = $('#master-table').dataTable().api();
      $('#master-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#master-table').DataTable().ajax.reload();
      });

      //dirrect to detail data
      function detail_data(id){
        window.location = '<?=base_url();?>'+'File2/detail_data/'+id;
      }

      //dirrect to edit data
      function edit_data(id){
        window.location = '<?=base_url();?>'+'File2/edit_data/'+id;
      }

      //delete data
      function delete_data(id){
        var url = '<?php echo site_url('File2/delete_data/') ?>'+id;
        // console.log(url);
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
      }
      // //Export Excel 
      // $(document).ready(function(){
      $('#laporan').click(function() {
          // var tanggal1    = $('#tanggal1').val();
          // var tanggal2    = $('#tanggal2').val();
          // var nopol       = $('#no_polisi').val();
          // var bahan_bakar = $('#bahan_bakar').val();
          var base_url    = "<?php echo base_url();?>";
          window.open(base_url+'File2/export_report_pemanfaatan/');
        });
      // })

      // sidebar
      function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
      }

      function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        document.body.style.backgroundColor = "white";
      }
  </script>

<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

</body>

</html>