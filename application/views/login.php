<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <style type="text/css">
    .tengah {
        margin: auto;
        position: absolute;
        top: 0; left: 0; bottom: 0; right: 0;
        height: 365px;
        /* border: 1px solid #999;  */
    }
  </style>
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center col-lg-11">

      <a href="#header" class="logo scrollto"><img src="main/img/jateng.png" alt="prov. jateng" class="img-fluid"></a>
      <h1 class="logo mr-auto" style="margin-left:10px;font-size:22px;"><a class="scrollto" style="color:#fff">Login Admin</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url('');?>" style="font-size:18px; color:#fff;"><i class="icofont-bubble-left"></i> Kembali</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="login" class="login">
      <div class="tengah" data-aos="fade-up">

        <div class="sub-title">
          <h2>Silahkan Login</h2>
        </div>
        
        <div class="container col-md-4 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
          <div class="total">
		        <div class="col-sm-12">
			        <div id="login-page">
                <form role="form" class="form-login" method="POST" id="form-login">
                <br>
                <div class="login-wrap">
                    <input type="text" class="form-control" onKeyup="this.value = this.value.toUpperCase()" placeholder="ID" name="id" id="id" autofocus required>
                    <br>
                    <input type="password" class="form-control" onKeyup="this.value = this.value.toUpperCase()" placeholder="Password" name="pass" id="pass">
                    <br>
                    <button class="btn btn-primary btn-block" href="" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                    <hr>
                    <div class="login-social-link centered">
                        <center><p>Lupa password? Silahkan hubungi admin!</p>
                    </div>
                </div>
                </form>
              </div>
			      </div>
          </div>
        </div>

      </div>
  <?php $this->load->view("partials/alert.php") ?>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->



  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <?php $this->load->view("partials/js.php") ?>
  <script>
    $('#form-login').submit(function(event) {
      event.preventDefault();
      var id = $('#id').val();
      var pass = $('#pass').val();

      if (!id) {
        $("#alert_warning_login").trigger("click");
        return false;
      }
      if (!pass) {
        $("#alert_warning_login").trigger("click");
        return false;
      }

      $.ajax({
        type: "POST",
        url: "Login/logon",
        data: $('#form-login').serialize(),
        success: function(response) {
          var result = JSON.parse(response);
          
          if (result.status == 422) {
            $("#alert_warning_login").trigger("click");
          }
          else{
            window.location = "<?php echo base_url('Login')?>";
          }
        },
        error: function(response) {
          $("#alert_warning_login").trigger("click");
        }
      });

    });
  </script>


</body>

</html>