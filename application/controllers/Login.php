<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login');
    }
    
    public function index()
	{
		// // die();
		$check_session = $this->session->userdata('PBSubK');
		
		// // var_dump($check_session);
		
		if ($check_session != null)  {
			redirect('File');
		} else {

			$this->load->view("login");
		}
    }
    
    public function logon($post=NULL)
	{
		$id     = $this->input->post("id");
		$pass   = $this->input->post('pass');

		$checking = $this->M_Login->check_user($id,$pass);

		if ($checking->num_rows() > 0) {
			foreach ($checking->result() as $apps) {
				
				$session_data = array(
					'Pengguna'      => $apps->Pengguna,
					'Sandi'     	=> $apps->Sandi,
					'PBSubK'		=> $apps->PBSubK,
					'NamaLengkap'	=> $apps->NamaLengkap,
				);
				//set session userdata
				$this->session->set_userdata($session_data);
				$status = 200;          
			}
		} else {
			$status = 422;
		}

		
		$data = array(
			'status'     => $status, 
		);
		echo json_encode($data);
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}