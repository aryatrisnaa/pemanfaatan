<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class File2 extends CI_Controller {

	function  __construct() {
        parent::__construct();
        //load our helper
        $this->load->helper('url');
        //load our model
        $this->load->model('files_model2');
    }

    public function index(){
            //load session library to use flashdata
        $this->load->library('session');
            //fetch all files i the database
        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('masterPemanfaatan/file_master2');
        }
        else{
            redirect("Login/logout");
        }
    }
    
    public function user(){
        //load session library to use flashdata
        $this->load->library('session');

        $data = array(
            // 'files'              => $this->files_model2->getAllFiles(),
            'bmd'                       => $this->files_model2->getBmd(),
            'jenisManfaat'              => $this->files_model2->getJenis(),
            'Peruntukan'                => $this->files_model2->getPeruntukan(),
            'bmd_selected'              => '',
            'JenisManfaat_selected'     => '',
            'Peruntukan_selected'       => '',
        );
            //fetch all files i the database
        // $data['files'] = $this->files_model2->getAllFiles();
        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('masterPemanfaatan/file_upload2', $data);
        }
        else{
            redirect("Login/logout");
        }
    
    }

    public function insert(){
		//load session library to use flashdata
      $this->load->library('session');

      $check_session = $this->session->userdata('PBSubK');
		
		if ($check_session != null) {

	 	    //Check if file is not empty
                    $config['upload_path']          = 'upload/manfaat/';
                    $config['allowed_types']        = 'pdf|jpg|jpeg|png';
                    $config['max_size']             = 5000;
                    // $config['max_width']            = 1024;
                    // $config['max_height']           = 768;
                    if($_FILES["fileSK"]["name"]){
                        $config["file_name"] = "SK_".$_FILES['fileSK']['name'];
                        $namaSK = $config["file_name"];
                        $this->load->library('upload', $config);
                        $fileSK = $this->upload->do_upload('fileSK');
                        if (!$fileSK){
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata("error", ".");
                        }else{
                            $uploadSK = $this->upload->data("file_name");
                            $data = array('upload_data' => $this->upload->data());
                            $this->session->set_flashdata("success", ".");
                        }
                    }
                
                    if($_FILES["filePerjanjian"]["name"]){
                        $config["file_name"] = "SP_".$_FILES['filePerjanjian']['name'];
                        $namaPerjanjian = $config["file_name"];
                        if($_FILES["fileSK"]["name"]){
                            $this->upload->initialize($config);
                        }else{
                            $this->load->library('upload', $config);
                        }
                        $filePerjanjian = $this->upload->do_upload('filePerjanjian');
                        if (!$filePerjanjian){
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata("error", "" );
                        }else{
                            $uploadPerjanjian = $this->upload->data("file_name");
                            $data = array('upload_data' => $this->upload->data());
                            $this->session->set_flashdata("success", ".");
                        }
                    }

                    if($_FILES["fileKTP"]["name"]){
                        $config["file_name"] = "KTP_".$_FILES['fileKTP']['name'];
                        $namaKTP = $config["file_name"];
                        if($_FILES["fileSK"]["name"]){
                            $this->upload->initialize($config);
                        }else{
                            $this->load->library('upload', $config);
                        }
                        if($_FILES["filePerjanjian"]["name"]){
                            $this->upload->initialize($config);
                        }else{
                            $this->load->library('upload', $config);
                        }
                        $fileKTP = $this->upload->do_upload('fileKTP');
                        if (!$fileKTP){
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata("error", "" );
                        }else{
                            $uploadKTP = $this->upload->data("file_name");
                            $data = array('upload_data' => $this->upload->data());
                            $this->session->set_flashdata("success", ".");
                        }
                    }

				//Load upload library and initialize configuration
				
                        $jenis          = $this->input->post('idJenis');
                        $idBMD          = $this->input->post('idBMD');
                        $Peruntuk       = $this->input->post('Peruntukan');
                        $now            = date('Y-m-d H:i:s');
                        $penginput      = $this->session->userdata('PBSubK');
                        $id             = $this->uuid->v4();
                        $luasTK         = $this->input->post('luasTK');
                        $luasBK         = $this->input->post('luasBK');

                        $data = array(
                            'id'         	    => $id,
                            'idBMD'   	        => $idBMD,
                            'luasTK'		    => $luasTK,
                            'luasBK'      	    => $luasBK,
                            'dasarSK'           => $this->input->post('dasarSK'),
                            'fileSK'            => $uploadSK,
                            'noKeputusan'       => $this->input->post('noKeputusan'),
                            'dasarPerjanjian'   => $this->input->post('dasarPerjanjian'),
                            'filePerjanjian'    => $uploadPerjanjian,
                            'noPerjanjian'      => $this->input->post('noPerjanjian'),
                            'idJenis'           => $jenis,
                            'namaPemohon'       => $this->input->post('namaPemohon'),
                            'alamatPemohon'     => $this->input->post('alamatPemohon'),
                            'noPemohon'         => $this->input->post('noPemohon'),
                            'fileKTP'           => $uploadKTP,
                            'jangkasewa'        => $this->input->post('jangkasewa'),
                            'tglMulai'          => date('d-m-Y',strtotime($this->input->post('tglMulai'))),
                            'tglSelesai'        => date('d-m-Y',strtotime($this->input->post('tglSelesai'))),
                            'idPeruntukan'      => $Peruntuk.".".$this->input->post('keterangan'),
                            'tgl_input'         => $now,
                            'pengguna'          => $penginput,

                        );            

                        $sisa = $this->files_model2->getSelisih($id);
                        $sisaT = $sisa->selisihT;
                        $sisaB = $sisa->selisihB;
                            if(($sisaT!= NULL && $sisaT < $luasTK) || ($sisaB!=NULL && $sisaB < $luasBK))
                            {
                                header('location:'.base_url("File2/user"));
                                $this->session->set_flashdata('error','Sisa Luasan BMD Tidak Mencukupi Untuk Dikerjasamakan.');
                            } else{
                                $query = $this->files_model2->insertfile2($data);
                                if($query){
                                    header('location:'.base_url("File2/detail_data/".$id));
                                    $this->session->set_flashdata('success','Data berhasil diunggah, Matursuwun.');
                                }
                                else{
                                    header('location:'.base_url("File2"));
                                    $this->session->set_flashdata('error','Data gagal tersimpan dalam database.');
                                }
                            }

                        
        } else {
        
            redirect('Login');
        }

    }


    public function insert_pembayaran(){
		//load session library to use flashdata
      $this->load->library('session');

      $check_session = $this->session->userdata('PBSubK');
		
		if ($check_session != null) {

			//Load upload library and initialize configuration
            $idPMF             = $this->input->post('idPMF');
            if($this->input->post('th1_nominal')==null){
                $nom1 = 0;
            }
            else{
                $nom1 = $this->input->post('th1_nominal');
            }
            if($this->input->post('th2_nominal')==null){
                $nom2 = 0;
            }
            else{
                $nom2 = $this->input->post('th2_nominal');
            }
            if($this->input->post('th3_nominal')==null){
                $nom3 = 0;
            }
            else{
                $nom3 = $this->input->post('th3_nominal');
            }
            if($this->input->post('th4_nominal')==null){
                $nom4 = 0;
            }
            else{
                $nom4 = $this->input->post('th4_nominal');
            }
            if($this->input->post('th5_nominal')==null){
                $nom5 = 0;
            }
            else{
                $nom5 = $this->input->post('th5_nominal');
            }
            $data = array(
                'idPembayaran'      => $this->uuid->v4(),
                'idPemanfaatan'     => $idPMF,
                'th1_nominal'		=> $nom1,
                'th1_tglTempo'      => $this->input->post('th1_tglTempo'),
                'th1_noSTS'         => $this->input->post('th1_noSTS'),
                'th1_tglSTS'        => $this->input->post('th1_tglSTS'),
                'th2_nominal'		=> $nom2,
                'th2_tglTempo'      => $this->input->post('th2_tglTempo'),
                'th2_noSTS'         => $this->input->post('th2_noSTS'),
                'th2_tglSTS'        => $this->input->post('th2_tglSTS'),
                'th3_nominal'		=> $nom3,
                'th3_tglTempo'      => $this->input->post('th3_tglTempo'),
                'th3_noSTS'         => $this->input->post('th3_noSTS'),
                'th3_tglSTS'        => $this->input->post('th3_tglSTS'),
                'th4_nominal'		=> $nom4,
                'th4_tglTempo'      => $this->input->post('th4_tglTempo'),
                'th4_noSTS'         => $this->input->post('th4_noSTS'),
                'th4_tglSTS'        => $this->input->post('th4_tglSTS'),
                'th5_nominal'		=> $nom5,
                'th5_tglTempo'      => $this->input->post('th5_tglTempo'),
                'th5_noSTS'         => $this->input->post('th5_noSTS'),
                'th5_tglSTS'        => $this->input->post('th5_tglSTS'),
                
            );            

            $query = $this->files_model2->insertPembayaran($data);
            if($query){
                header('location:'.base_url("File2/detail_data/".$idPMF));
                $this->session->set_flashdata('success','Data Pembayaran Berhasil DIperbaharui, Matursuwun.');
            }
            else{
                header('location:'.base_url("File2/detail_data/".$idPMF));
                $this->session->set_flashdata('error','Data gagal tersimpan dalam database.');
            }
        } else {
        
            redirect('Login');
        }

    }

    public function download($id){
        $this->load->helper('download');
        $fileinfo = $this->files_model2->download($id);
        $file = 'upload/'.$fileinfo['nama'];
        force_download($file, NULL);
    }

    //detail
    public function detail_data($id)
    {
        $this->load->library('session');
        $query = $this->db->query("SELECT a.statusBMD, a.jenisObjek, a.kondisiBMD, a.alamatBMD, c.KetWil, a.luasT, a.luasB, 
                a.nilaiWajar, a.photoA, a.photoB, a.photoC, a.photoD, a.latitude, a.longtitude, b.*, (d.ketPeruntukan + ' ' + SUBSTRING(b.idPeruntukan,7,100)) as Peruntukan, e.KetJenis, f.*
                FROM AsetInfo.dbo.M_MasterBMD a 
                INNER JOIN AsetInfo.dbo.M_Pemanfaatan b ON b.idBMD=a.id 
                INNER JOIN AsetInfo.dbo.ListKota c ON a.kota =c.KdWil 
                INNER JOIN AsetInfo.dbo.M_Peruntukan d ON left(b.idPeruntukan,5)=d.idPeruntukan
                inner join AsetInfo.dbo.JenisManfaat e on e.KdJenis=b.idJenis 
                left join AsetInfo.dbo.M_Pembayaran f on f.idPemanfaatan = b.id where b.id = '$id'")->row();
    
        $data["detail2"] = $query;
        // echo json_encode($data);
    
        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('masterPemanfaatan/file_detail2', $data);
        }
        else{
            redirect("Login/logout");
        }
        // return $query;
    }

    // Edit DATA
    public function edit_data($id)
	{
        $this->load->library('session');
		$query = $this->db->query("SELECT *, (d.ketPeruntukan + ' ' + SUBSTRING(b.idPeruntukan,7,100)) as Peruntukan FROM AsetInfo.dbo.M_MasterBMD a 
        INNER JOIN AsetInfo.dbo.M_Pemanfaatan b ON b.idBMD=a.id
        INNER JOIN AsetInfo.dbo.ListKota c ON a.kota =c.KdWil 
        INNER JOIN AsetInfo.dbo.M_Peruntukan d ON left(b.idPeruntukan,5)=d.idPeruntukan
        inner join AsetInfo.dbo.JenisManfaat e on e.KdJenis=b.idJenis where b.id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			// 'query' => $query,
            'bmd'                       => $this->files_model2->getBmd(),
            'jenisManfaat'              => $this->files_model2->getJenis(),
            'Peruntukan'                => $this->files_model2->getPeruntukan(),
            'bmd_selected'              => '',
            'JenisManfaat_selected'     => '',
            'Peruntukan_selected'       => '',
		);
        $data["edit"] = $query;
		// echo json_encode($data);

        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('masterPemanfaatan/file_upload_edit2', $data);
        }
        else{
            redirect("Login/logout");
        }
		// return $query;
	}

	public function update_data()
	{
        $id             = $this->input->post('edit_id2');
		$jenis          = $this->input->post('edit_idJenis');
        $idBMD          = $this->input->post('edit_idBMD');
        $Peruntuk       = $this->input->post('edit_Peruntukan');
        $data = array(
            'idBMD'   	        => $idBMD,
            'luasTK'		    => $this->input->post('edit_luasTK'),
            'luasBK'      	    => $this->input->post('edit_luasBK'),
            'dasarSK'           => $this->input->post('edit_dasarSK'),
            // 'fileSK'            => $namaSK,
            'noKeputusan'       => $this->input->post('edit_noKeputusan'),
            'dasarPerjanjian'   => $this->input->post('edit_dasarPerjanjian'),
            // 'filePerjanjian'    => $namaPerjanjian,
            'noPerjanjian'      => $this->input->post('edit_noPerjanjian'),
            'idJenis'           => $jenis,
            'namaPemohon'       => $this->input->post('edit_namaPemohon'),
            'alamatPemohon'     => $this->input->post('edit_alamatPemohon'),
            'noPemohon'         => $this->input->post('edit_noPemohon'),
            // 'fileKTP'           => $namaKTP,
            'jangkasewa'        => $this->input->post('edit_jangkasewa'),
            'tglMulai'          => date('d-m-Y',strtotime($this->input->post('edit_tglMulai'))),
            'tglSelesai'        => date('d-m-Y',strtotime($this->input->post('edit_tglSelesai'))),
            'idPeruntukan'      => $Peruntuk,
        );   
				
		$this->db->where('id', $id);
		$this->db->update('M_Pemanfaatan', $data);
		header('location:'.base_url("File2/detail_data/".$id));
		$this->session->set_flashdata('success','Data berhasil disimpan');
	}

    public function delete_data($id=null)
    {   
		$check_session = $this->session->userdata('PBSubK');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$where = array('id' => $id);
                $this->files_model2->delete_data($where,'asetinfo.dbo.M_Pemanfaatan');
						
				header('location:'.base_url("File2"));
				$this->session->set_flashdata('success','Data berhasil dihapus!');
			}
		} else {

			redirect('login');
		}
	}

    public function data_master2()
	{
		// $columns = array(
		// 	0 => 'tgl_input'
		// );

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		// $order = $columns[$this->input->post('order')[0]['column']];
		$dir   = ' DESC ';
		$draw  = $this->input->post('draw');

		$totalData = $this->files_model2->all_master_count2();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->files_model2->all_master_data2($limit, $start, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->files_model2->search_master_data2($limit, $start, $dir, $search);

			$totalFiltered = $this->files_model2->search_master_count2($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp                  = '"'.$value->id.'"';
				$nestedData['statusBMD']  = ucwords($value->statusBMD);
                $nestedData['KetJenis']  = ucwords($value->KetJenis);
                $nestedData['jenisObjek']  = ucwords($value->jenisObjek);
				$nestedData['alamatBMD']  = ucwords($value->alamatBMD);
				$nestedData['KetWil']  = ucwords($value->KetWil);
				$nestedData['luasTK']  = ucwords($value->luasTK);
                $nestedData['luasBK']  = ucwords($value->luasBK);
                $nestedData['namaPemohon']  = ucwords($value->namaPemohon);
                // $nestedData['alamatPemohon']  = ucwords($value->alamatPemohon);
                $nestedData['jangkasewa']  = ucwords($value->jangkasewa).' Tahun';
                $nestedData['ketPeruntukan']  = ucwords($value->Peruntukan);
                // $nestedData['totalNilai']  = ucwords($value->totalNilai);
                $nestedData['action']   = "<div class='btn-group'>
                                            <a onclick='return edit_data($_temp)' class='btn btn-sm btn-primary' style='color:#fff;'><i class='icofont-pencil'> </i> Edit</a>
                                            <a onclick='return detail_data($_temp)' class='btn btn-sm btn-primary' style='color:#fff;'><i class='icofont-info'> </i> Detail</a>
                                            <a onclick='return delete_data($_temp)' class='btn btn-sm btn-danger' style='color:#fff;'><i class='icofont-trash'> </i></a>
                                            </div>
                                            ";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

    // POST SELISIH
    public function get_selisih(){
        $id=$this->input->post('id');
        $data=$this->Files_model2->get_Turunan($id);
        echo json_encode($data);
    }

    public function getluasT(){
        $id = $this->input->post('idBMD');

        $hasil=$this->db->query("SELECT a.id, a.alamatBMD, a.luasT, a.luasB, sum (b.luasTK) luasTK, sum(b.luasBK) luasBK, 
		a.luasT - (sum (b.luasTK)) as selisihT,  a.luasB - (sum (b.luasBK)) as selisihB
        from AsetInfo.dbo.M_MasterBMD a 
        left join AsetInfo.dbo.M_Pemanfaatan b on a.id=b.idBMD
		where a.id = '$id' group by a.id, a.luasT, a.alamatBMD, a.luasB")->row();

        $data = array(
            'luasT'     => $hasil->luasT,
            'luasB'     => $hasil->luasB,
            'selisihT'  => $hasil->selisihT,
            'selisihB'  => $hasil->selisihB,
        );
        echo json_encode($data);

    }

    public function export_report_pemanfaatan(){
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();;

		$sheet->setCellValue('A1', "NO")->setBold(true); 
		$sheet->setCellValue('B1', "Status BMD"); 
		$sheet->setCellValue('C1', "Jenis Objek"); 
		$sheet->setCellValue('D1', "Kondisi"); 
		$sheet->setCellValue('E1', "Alamat"); 
		$sheet->setCellValue('F1', "Kota"); 
		$sheet->setCellValue('G1', "Luas Tanah Pemanfaatan"); 
		$sheet->setCellValue('H1', "Luas Bangunan Pemanfaatan"); 
		$sheet->setCellValue('I1', "Dasar SK"); 
        $sheet->setCellValue('J1', "No Keputusan"); 
        $sheet->setCellValue('K1', "Dasar Perjanjian"); 
        $sheet->setCellValue('L1', "No. Perjanjian");
        $sheet->setCellValue('M1', "Jenis Pemanfaatan");
        $sheet->setCellValue('N1', "Nama Pemohon");
        $sheet->setCellValue('O1', "Alamat Pemohon");
        $sheet->setCellValue('P1', "No. Pemohon"); 
        $sheet->setCellValue('Q1', "Jangka Waktu");
        $sheet->setCellValue('R1', "Tgl Mulai");
        $sheet->setCellValue('S1', "Tgl Berakhir");
        $sheet->setCellValue('T1', "Penggunaan");

		$details = $this->files_model2->export_report_pemanfaatan();

		$no     = 1;
		$numrow = 2;
		foreach ($details as $key => $detail) {

		  $sheet->setCellValue('A'.$numrow, $no);
          $sheet->setCellValue('B'.$numrow, ucwords($detail->statusBMD));
		  $sheet->setCellValue('C'.$numrow, ucwords($detail->jenisObjek));
		  $sheet->setCellValue('D'.$numrow, ucwords($detail->kondisiBMD));
		  $sheet->setCellValue('E'.$numrow, ucwords($detail->alamatBMD));
		  $sheet->setCellValue('F'.$numrow, ucwords($detail->KetWil));
		  $sheet->setCellValue('G'.$numrow, ucwords($detail->luasTK).' m2');
		  $sheet->setCellValue('H'.$numrow, ucwords($detail->luasBK).' m2');
          $sheet->setCellValue('I'.$numrow, ucwords($detail->dasarSK));
          $sheet->setCellValue('J'.$numrow, ucwords($detail->noKeputusan));
          $sheet->setCellValue('K'.$numrow, ucwords($detail->dasarPerjanjian));
          $sheet->setCellValue('L'.$numrow, ucwords($detail->noPerjanjian));
          $sheet->setCellValue('M'.$numrow, ucwords($detail->KetJenis));
          $sheet->setCellValue('N'.$numrow, ucwords($detail->namaPemohon));
          $sheet->setCellValue('O'.$numrow, ucwords($detail->alamatPemohon));
          $sheet->setCellValue('P'.$numrow, ucwords($detail->noPemohon));
          $sheet->setCellValue('Q'.$numrow, ucwords($detail->jangkasewa).' tahun');
          if($detail->tglMulai != NULL){
              $sheet->setCellValue('R'.$numrow, date('Y-m-d',strtotime($detail->tglMulai)));
          }
          if($detail->tglSelesai != NULL){
              $sheet->setCellValue('S'.$numrow, date('Y-m-d',strtotime($detail->tglSelesai)));
          }
          $sheet->setCellValue('T'.$numrow, ucwords($detail->Peruntukan));
		  $no++;
		  $numrow++;
		}

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(false);
		$sheet->getColumnDimension('H')->setAutoSize(false);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
		$sheet->getColumnDimension('N')->setAutoSize(true);
		$sheet->getColumnDimension('O')->setAutoSize(true);
		$sheet->getColumnDimension('P')->setAutoSize(true);
		$sheet->getColumnDimension('Q')->setAutoSize(false);
		$sheet->getColumnDimension('R')->setAutoSize(true);
		$sheet->getColumnDimension('S')->setAutoSize(true);
		$sheet->getColumnDimension('T')->setAutoSize(true);
		
		$sheet->getDefaultRowDimension()->setRowHeight(-1);

		$writer   = new Xlsx($spreadsheet);
		$filename = 'Laporan Pemanfaatan';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
		
		ob_end_clean();+
		$writer->save('php://output');
	}
}
