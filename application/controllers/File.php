<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller {

	function  __construct() {
        parent::__construct();
        //load our helper
        $this->load->helper('url');
        //load our model
        $this->load->model('Files_model');
    }

    public function index(){
            //load session library to use flashdata
        $this->load->library('session');
            //fetch all files i the database
        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('master/file_master');
        }
        else{
            redirect("Login/logout");
        }
    }
    
    public function user(){
        //load session library to use flashdata
        $this->load->library('session');

        $data = array(
            // 'files'              => $this->Files_model->getAllFiles(),
            'kota'              => $this->Files_model->getWil(),
            'status'              => $this->Files_model->getjenis(),
            'kota_selected'   => '',
            'JenisManfaat_selected'   => '',
        );
            //fetch all files i the database
        // $data['files'] = $this->Files_model->getAllFiles();
        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('master/file_upload', $data);
        }
        else{
            redirect("Login/logout");
        }
    
    }

    // tes

    public function insert(){
		//load session library to use flashdata
      $this->load->library('session');

      $check_session = $this->session->userdata('PBSubK');
		
		if ($check_session != null) {

	 	//Check if file is not empty
				$config['upload_path'] = 'upload/master/images/';
				//restrict uploads to this mime types
				$config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size']      = '0';
				// $config['file_name_1'] = $_FILES['photoA']['name'];
				// $config['file_name_2'] = $_FILES['photoB']['name'];
				
                $dataInfo = array();
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for($i=0; $i<$cpt; $i++)
                {           
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i]; 
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];  
            
                    $this->load->library('upload', $config);
    				$this->upload->initialize($config); 
                    $this->upload->do_upload('userfile');
                    $dataInfo[] = $this->upload->data();
                }

				//Load upload library and initialize configuration
				
                        // $uploadData[] = $this->upload->data();
						// $datafotoA = $uploadData['file_name_1'];
                        // $datafotoB = $uploadData['file_name_2'];
                        $kdwil          = $this->input->post('kota');
                        $JenisManfaat   = $this->input->post('status');
                        $now            = date('Y-m-d H:i:s');

                        $data = array(
                            'id'         	    => $this->uuid->v4(),
                            'statusBMD'   	    => $this->input->post('statusBMD'),
                            // 'tgl_surat'     => date('d-m-Y',strtotime($this->input->post('surat_tanggal'))),
                            'jenisObjek'		=> $this->input->post('jenisObjek'),
                            'kondisiBMD'      	=> $this->input->post('kondisiBMD'),
                            'KIBA'      	    => $this->input->post('KIBA'),
                            'KIBC'            	=> $this->input->post('KIBC'),
                            'kondisiBMD'      	=> $this->input->post('kondisiBMD'),
                            'alamatBMD'         => $this->input->post('alamatBMD'),
                            'kota'              => $kdwil,
                            'luasT'             => $this->input->post('luasT'),
                            'luasB'             => $this->input->post('luasB'),
                            'nilaiWajar'        => $this->input->post('nilaiWajar'),
                            'photoA' 	        => $dataInfo[0]['file_name'],
                            'photoB' 	        => $dataInfo[1]['file_name'],
                            'photoC' 	        => $dataInfo[2]['file_name'],
                            'photoD' 	        => $dataInfo[3]['file_name'],
                            'latitude'          => $this->input->post('latitude'),
                            'longtitude'        => $this->input->post('longtitude'),
                            'longtitude'        => $this->input->post('longtitude'),
                            'tgl_input'         => $now,

                            // 'photoC' 	    => $dataInfo[2]['file_name'],
                            // 'photoD' 	    => $dataInfo[3]['file_name'],
                            // 'created_by' 	=> $penginput,
                            // 'created_at' 	=> $now,
                        );            

                        $query = $this->Files_model->insertfile($data);
                        if($query){
                            header('location:'.base_url("File"));
                            $this->session->set_flashdata('success','Data berhasil diunggah, Matursuwun.');
                        }
                        else{
                            header('location:'.base_url("File"));
                            $this->session->set_flashdata('error','Data gagal tersimpan dalam database.');
                        }
        } else {
        
            redirect('Login');
        }

    }

    //detail
    public function detail_data($id)
	{
        $this->load->library('session');
		$query = $this->db->query("SELECT * from AsetInfo.dbo.M_MasterBMD a left join AsetInfo.dbo.ListKota b on a.kota=b.KdWil where id = '$id'")->row();

        $data["detail"] = $query;
		// echo json_encode($data);

        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('master/file_detail', $data);
        }
        else{
            redirect("Login/logout");
        }
		// return $query;
	}

    // Edit DATA
    public function edit_data($id)
	{
        $this->load->library('session');
		$query = $this->db->query("SELECT * from AsetInfo.dbo.M_MasterBMD a left join AsetInfo.dbo.ListKota b on a.kota=b.KdWil where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			// 'query' => $query,
            'kota'              => $this->Files_model->getWil(),
            // 'status'              => $this->Files_model->getjenis(),
            'kota_selected'   => '',
            // 'JenisManfaat_selected'   => '',
		);
        $data["edit"] = $query;
		// echo json_encode($data);

        $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('master/file_upload_edit', $data);
        }
        else{
            redirect("Login/logout");
        }
		// return $query;
	}

	public function update_data()
	{
		$id   = $this->input->post('edit_id');
        $kdwil          = $this->input->post('edit_kota');
		$data = array(
            'statusBMD'   	    => $this->input->post('edit_statusBMD'),
            // 'tgl_surat'     => date('d-m-Y',strtotime($this->input->post('surat_tanggal'))),
            'jenisObjek'		=> $this->input->post('edit_jenisObjek'),
            'kondisiBMD'      	=> $this->input->post('edit_kondisiBMD'),
            'KIBA'           	=> $this->input->post('edit_KIBA'),
            'KIBC'           	=> $this->input->post('edit_KIBC'),
            'alamatBMD'         => $this->input->post('edit_alamatBMD'),
            'kota'              => $kdwil,
            'luasT'             => $this->input->post('edit_luasT'),
            'luasB'             => $this->input->post('edit_luasB'),
            'nilaiWajar'        => $this->input->post('edit_nilaiWajar'),
            // 'photoA' 	        => $dataInfo[0]['file_name'],
            // 'photoB' 	        => $dataInfo[1]['file_name'],
            'latitude'          => $this->input->post('edit_latitude'),
            'longtitude'        => $this->input->post('edit_longtitude'),
		);
				
		$this->db->where('id', $id);
		$this->db->update('M_MasterBMD', $data);
		header('location:'.base_url("File"));
		$this->session->set_flashdata('success','Data berhasil disimpan');
	}


    public function delete_data($id=null)
    {   
		$check_session = $this->session->userdata('PBSubK');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$where = array('id' => $id);
                $this->Files_model->delete_data($where,'asetinfo.dbo.M_MasterBMD');
						
				header('location:'.base_url("File"));
				$this->session->set_flashdata('success','Data berhasil dihapus!');
			}
		} else {

			redirect('login');
		}
	}

    public function data_master()
	{
		// $columns = array(
		// 	0 => 'id'
		// );

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		// $order = $columns[$this->input->post('order')[0]['column']];
		$dir   = 'DESC';
		$draw  = $this->input->post('draw');

		$totalData = $this->Files_model->all_master_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Files_model->all_master_data($limit, $start, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Files_model->search_master_data($limit, $start, $dir, $search);

			$totalFiltered = $this->Files_model->search_master_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp                  = '"'.$value->id.'"';
				$nestedData['statusBMD']  = ucwords($value->statusBMD);
				$nestedData['jenisObjek']  = ucwords($value->jenisObjek);
				$nestedData['kondisiBMD']  = ucwords($value->kondisiBMD);
				$nestedData['alamatBMD']  = ucwords($value->alamatBMD);
				$nestedData['kota']  = ucwords($value->kota);
                $nestedData['luasT']  = ucwords($value->luasT).' m2';
                $nestedData['luasB']  = ucwords($value->luasB).' m2';
				$nestedData['nilaiWajar']  = 'Rp.'.number_format($value->nilaiWajar ,2,",",".");
                $nestedData['action']   = "<div class='btn-group'>
                                            <a onclick='return edit_data($_temp)' class='btn btn-sm btn-primary' style='color:#fff;'><i class='icofont-pencil'> </i> Edit</a>
                                            <a onclick='return detail_data($_temp)' class='btn btn-sm btn-primary' style='color:#fff;'><i class='icofont-info'> </i> Detail</a>
                                            <a onclick='return delete_data($_temp)' class='btn btn-sm btn-danger' style='color:#fff;'><i class='icofont-trash'> </i></a>
                                            </div>
                                            ";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

    public function idle(){
    //load session library to use flashdata
    $this->load->library('session');
    //fetch all files i the database
    $akun = $this->session->userdata('PBSubK');
        if($akun != null){
            $this->load->view('idle/index');
        }
        else{
            redirect("Login/logout");
        }
    }

    public function data_idle()
	{
		$columns = array(
			0 => 'id'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = $this->input->post('order')[0]['dir'];
		$draw  = $this->input->post('draw');

		$totalData = $this->Files_model->all_idle_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->Files_model->all_idle_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->Files_model->search_idle_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->Files_model->search_idle_count($search);
		}

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp                  = '"'.$value->id.'"';
				$nestedData['statusBMD']  = ucwords($value->statusBMD);
				$nestedData['jenisObjek']  = ucwords($value->jenisObjek);
				$nestedData['kondisiBMD']  = ucwords($value->kondisiBMD);
				$nestedData['alamatBMD']  = ucwords($value->alamatBMD);
				$nestedData['kota']  = ucwords($value->KetWil);
                $nestedData['luasT']  = ucwords($value->luasT).' m2';
                if($value->sisaT == null){
                    $nestedData['sisaT']  = ucwords($value->luasT).' m2';
                }
                else{
                    $nestedData['sisaT']  = ucwords($value->sisaT).' m2';
                }
                $nestedData['luasB']  = ucwords($value->luasB).' m2';
                if($value->sisaB == null){
                    $nestedData['sisaB']  = ucwords($value->luasB).' m2';
                }
                else{
                    $nestedData['sisaB']  = ucwords($value->sisaB).' m2';
                }
				$nestedData['nilaiWajar']  = 'Rp.'.number_format($value->nilaiWajar ,2,",",".");
                $nestedData['action']   = "<div class='btn-group'>
                                            <a onclick='return detail_data($_temp)' class='btn btn-sm btn-primary' style='color:#fff;'><i class='icofont-info'> </i> Detail</a>
                                            </div>
                                            ";

				$data[] = $nestedData;
				$nomor_urut++;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

}
