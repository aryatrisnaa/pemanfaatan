<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Home extends CI_Controller {
 
    function __construct(){
        parent::__construct();
        $this->load->model('Index_model');
    }
 
    public function index()
    {
		// var_dump($asd);
		$lokasiData = $this->Index_model->show_lokasiData()->result();
		$x["lokasi"] = $lokasiData;

		$rekap = $this->Index_model->rekap_jml()->row();
		$x["rekap"] = $rekap;
	
		// $data = array(
		//     'getFilter' => $this->M_Profil_A->show_status($status)->result()
		// );
		$this->load->view('index',$x);
    }

	
    // public function data_wilayah()
	// {
	// 	$columns = array(
	// 		0 => 'KdWil'
	// 	);

	// 	$limit = $this->input->post('length');
	// 	$start = $this->input->post('start');
	// 	$order = $columns[$this->input->post('order')[0]['column']];
	// 	$dir   = $this->input->post('order')[0]['dir'];
	// 	$draw  = $this->input->post('draw');

	// 	$totalData = $this->Index_model->all_kota_count();

	// 	$totalFiltered = $totalData;

	// 	if (empty($this->input->post('search')['value'])) {
	// 		$master = $this->Index_model->all_kota_data($limit, $start, $order, $dir);
	// 	} else {
	// 		$search = $this->input->post('search')['value'];

	// 		$master =  $this->Index_model->search_kota_data($limit, $start, $order, $dir, $search);

	// 		$totalFiltered = $this->Index_model->search_kota_count($search);
	// 	}

	// 	$data       = array();
	// 	$nomor_urut = 0;
	// 	if (!empty($master)) {
	// 		foreach ($master as $key => $value) {
	// 			$_temp                  = '"'.$value->KdWil.'"';
	// 			$nestedData['KetWil']  = ucwords($value->KetWil);
	// 			$nestedData['Sewa']  = ucwords($value->Sewa);
	// 			$nestedData['BGSBSG']  = ucwords($value->BGSBSG);
	// 			$nestedData['PinjamPakai']  = ucwords($value->PinjamPakai);
	// 			$nestedData['KSPKSPi']  = ucwords($value->KSPKSPi);
	// 			$nestedData['BlmOptimal']  = ucwords($value->BlmOptimal);
	// 			$nestedData['action']   = "<a onclick='return data_sub($_temp)' class='btn btn-sm btn-primary' style='color:#fff;'><i class='icofont-listine-dots'> </i> Detail</a>";

	// 			$data[] = $nestedData;
	// 			$nomor_urut++;
	// 		}
	// 	}

	// 	$json_data = array(
	// 		"draw"            => intval($this->input->post('draw')),
	// 		"recordsTotal"    => intval($totalData),
	// 		"recordsFiltered" => intval($totalFiltered),
	// 		"data"            => $data
	// 	);

	// 	echo json_encode($json_data);
	// }

	

}