<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Sub extends CI_Controller {
 
  function __construct(){
      parent::__construct();
      $this->load->model('Index_model');
  }
 
    public function index()
    {

        $data["sum"] = $this->Index_model->getProv();
        $data["det"] = $this->Index_model->detObjek();

		$this->load->view('subKab', $data);
    }

}